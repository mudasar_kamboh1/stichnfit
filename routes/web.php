<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Web\PageController@index')->name('home');
//Route::get('/', function (){
//    \Illuminate\Support\Facades\Auth::loginUsingId(2);
//});
Route::post('order', 'Web\OrderController@store');
Route::post('contact', 'Web\ContactController@store');
Route::get('check-track-otp', 'UserController@checkOrderTrakOtp');
Route::get('send-track-otp', 'UserController@sendTrackOtp');
Route::get('admin/login', 'WelcomeController@index')->name('welcome');
Route::get('gallery', 'Web\PageController@gallery');
Route::get('gallery/{subcategory}', 'Web\PageController@gallerySubcategory');
Route::get('gallery/{subcategory}/{post}', 'Web\PageController@galleryDetail');
Route::get('blog', 'Web\BlogController@index');
//Route::get('blog/{subcategory}', 'Web\BlogController@blogSubcategory');
Route::get('blog/{post}', 'Web\BlogController@blogDetail');

Route::group(['middleware' => 'CheckUserLoggedIn'], function () {
    Route::post('update-pickup-time', 'Web\OrderController@pickUpSchedule');
    Route::get('new-order', 'Web\OrderController@newOrder');
    Route::get('order/thank-you', 'Web\OrderController@thankYou')->name('thankyou');
    Route::get('order-detail/{token}', 'Web\OrderController@orderDetails');
    Route::get('track-order', 'Web\OrderController@trackOrder')->name('trackOrder');
    Route::get('my-orders', 'Web\OrderController@myOrders')->name('myOrders');
    Route::resource('webroders', 'Web\OrderController');
    Route::get('logout', function(\Illuminate\Http\Request $request){
        \Illuminate\Support\Facades\Auth::logout();
        return redirect('/');
    });
});
Route::get('privacy-policy', function (){
    return view('web.privacy-policy');
});
Route::get('about-us', function (){
    return view('web.about-us');
});
Route::get('how-we-work', function (){
    return view('web.how-we-work');
});
Route::get('faq', function (){
    return view('web.faq');
});
Route::get('contact-us', function (){
    return view('web.contact');
});
Route::get('404', function (){
    return view('web.single-gallery');
});

Route::get('profile','Admin\UsersController@show');
Route::post('updateprofile','Admin\UsersController@update');
//Route::get('welcome', 'welcomeController');
/**
 * Register the typical authentication routes for an application.
 * Replacing: Auth::routes();
 */
Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
    // Registration Routes...
    if (config('stitchandfit.registration_open')) {
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
        Route::post('register', 'RegisterController@register');
    }
    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');
    if (config('stitchandfit.impersonate')) {
        /**
         * Impersonate User. Requires authentication.
         */
        Route::post('impersonate/{id}', 'ImpersonateController@impersonate')->name('impersonate');
        /**
         * Stop Impersonate. Requires authentication.
         */
        Route::get('impersonate/stop', 'ImpersonateController@stopImpersonate')->name('impersonate.stop');
    }
});
// Redirect to /dashboard
Route::get('/home', 'HomeController@index')->name('home');
/**
 * Requires authentication.
 */
Route::group(['middleware' => 'auth'], function () {
    /**
     * Dashboard. Common access.
     * // Matches The "/dashboard/*" URLs
     */
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard::'], function () {
        /**
         * Dashboard Index
         * // Route named "dashboard::index"
         */
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

        /**
         * Profile
         * // Route named "dashboard::profile"
         */
        Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@showProfile']);
        Route::post('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@updateProfile']);
    });
    /**
     * // Matches The "/admin/*" URLs
     */
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin::'], function () {
        /**
         * Admin Access
         */
        Route::group(['middleware' => 'admin'], function () {
            /**
             * Admin Index
             * // Route named "admin::index"
             */
            Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);
            /**
             * Manage Users.
             * // Routes name "admin.users.*"
             */
            Route::resource('users', 'UsersController');
            Route::resource('orders', 'OrdersController');
            Route::resource('measurement', 'MeasurementController');
            Route::resource('gallery', 'GalleryController');
            Route::resource('category', 'CategoriesController');
            Route::resource('subcategory', 'SubCategoryController');
            Route::resource('contacts', 'ContactController');
            Route::resource('subCategory', 'SubCategoryController');
            Route::get('subCat/{id}', 'SubCategoryController@getCats');
            Route::get('orders/details/{id}', 'OrdersController@getOrderDetails');
        });
    });
});
