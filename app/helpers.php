<?php

use Illuminate\Http\Request;
use Illuminate\Support\HtmlString;

if (! function_exists('cdn_asset')) {
    /**
     * Generate a cdn asset path.
     *
     * @param string $path
     *
     * @return string
     */
    function cdn_asset(string $path)
    {
        return \App\Utils::cdnAsset($path);
    }
}

if (! function_exists('trustedproxy_config')) {
    /**
     * Get Trusted Proxy value
     *
     * @param string $key
     * @param string $env_value
     *
     * @return mixed
     */
    function trustedproxy_config($key, $env_value)
    {
        if ($key === 'proxies') {
            if ($env_value === '*' || $env_value === '**') {
                return $env_value;
            }

            return $env_value ? explode(',', $env_value) : null;
        } elseif ($key === 'headers') {
            if ($env_value === 'HEADER_X_FORWARDED_AWS_ELB') {
                return Request::HEADER_X_FORWARDED_AWS_ELB;
            } elseif ($env_value === 'HEADER_FORWARDED') {
                return Request::HEADER_FORWARDED;
            }

            return Request::HEADER_X_FORWARDED_ALL;
        }

        return null;
    }
}

if (! function_exists('redirect_back_field')) {
    /**
     * Generate a redirect back url form field.
     *
     * @return \Illuminate\Support\HtmlString
     */
    function redirect_back_field()
    {
        return new HtmlString('<input type="hidden" name="_redirect_back" value="'.old('_redirect_back', back()->getTargetUrl()).'">');
    }
}

if (! function_exists('redirect_back_to')) {
    /**
     * Get an instance of the redirector.
     *
     * @param  string|null  $callbackUrl
     * @param  int     $status
     * @param  array   $headers
     * @param  bool    $secure
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function redirect_back_to($callbackUrl = null, $status = 302, $headers = [], $secure = null)
    {
        $to = request()->input('_redirect_back', back()->getTargetUrl());
        if ($callbackUrl) {
            if (! starts_with($to, $callbackUrl)) {
                $to = $callbackUrl;
            }
        }

        return redirect($to, $status, $headers, $secure);
    }
}

if (!function_exists('sendSms')) {


    function sendSms($to, $message) {
        try {
            // Configuration variables
            $type = "xml";
            $lang = "English";
            $mask = "stitchnfit";
            $id = config('stitchandfit.SMS_USER');
            $pass = config('stitchandfit.SMS_PASS');

            $responseType[0] = '';
            $sendMessage = false;

            if ($to) {
                $cntNumber = strlen($to);
                $firstTwoChar = substr($to, 0, 2);

                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'staging' || env('APP_ENV') == null){
                    $to = config('stitchandfit.SMS_NUMBER_ZEESH');
                }else{
                    if ($cntNumber == 11 && $firstTwoChar == '03') {
                        $to = '92' . ltrim($to, "0");
                    }
                }
                $sendMessage = true;
            }
            // Data for text message
            if ($sendMessage && !is_null($message)) {

                $message = urlencode($message);
                // Prepare data for POST request
                $data = "id=" . $id . "&pass=" . $pass . "&msg=" . $message . "&to=" . $to . "&lang=" . $lang . "&mask=" . $mask . "&type=" . $type;
                // Send the POST request with cURL
                $ch = \curl_init('http://www.outreach.pk/api/sendsms.php/sendsms/url');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $result = curl_exec($ch); //This is the result from Outreach
                curl_close($ch);

                $response = simplexml_load_string($result);
                $responseType = array((string) $response->type);
                if($responseType[0] == 'Success'){
                    return true;
                } else {
                    $response_message = array((string) $response->response);
                    $prePareLog = $responseType[0] . ' Reason ' . $response_message[0] . ' On date ' . \Carbon\Carbon::now() . ' Message ' . $message . ' To ' . $to;
                    file_put_contents(storage_path('logs/sms_log.log'), $prePareLog, FILE_APPEND);
                    return false;
                }
            }else{
                return false;
            }
        } catch (\Exception $e) {
            error_log($e);
            return false;
        }
    }
}


if (!function_exists('uniqueOtp')) {

    function uniqueOtp($otp) {
        $user = \App\User::whereOtp($otp)->first();
        if (!is_null($user)) {
            $otp = uniqueOtp(rand(100, 999));
        }
        return $otp;
    }
}

if (!function_exists('uniqueOrderToken')) {

    function uniqueOrderToken($token) {
        $user = \App\Order::whereToken($token)->first();
        if(!is_null($user)) {
            $token = uniqueOrderToken(rand(100000, 999999));
        }
        return sprintf('%04d', $token);
    }
}


if (!function_exists('imageUpload')) {

    function imageUpload(Request $request, $path, $oldImagePath = null) {
        if(!is_null($oldImagePath)){
            if (file_exists($oldImagePath)) {
                @unlink($oldImagePath);
            }
        }
        $file = $request->file('image');
        $filename =  time() . '.' . $file->getClientOriginalExtension();
        $destinationPath = $path;
        $file->move($destinationPath,$filename);
        return $filename;
    }
}

if (!function_exists('makeSlug')) {

    function makeSlug($title, $model) {
        $slug = str_slug($title,'-');
        $slugCount = count($model->whereRaw("slug REGEXP '^{$slug}(-[0-9]*)?$'")->get());
        return ($slugCount > 0) ? "{$slug}-{$slugCount}" : $slug;
    }
}

if (!function_exists('makeNotificationText')) {

    function makeNotificationText($orderToken) {

    }
}