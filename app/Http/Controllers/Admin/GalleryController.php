<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Gallery::with('sub_category')->get();

        return view('admin.gallery.list',compact('lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gallery.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
        ]);

        $input = $request->all();
        $input['slug'] = makeSlug($input['name'], new Gallery());
        //Multi Images
        $images = [];
        if($files=$request->file('images')){
            foreach($files as $k => $file){
                $name = time() .$k. '.' . $file->getClientOriginalExtension();
                $file->move('assets/uploads',$name);
                $images[$k] = $name;
            }
            $input['images'] = implode(',',$images);

        }
        $input['image'] = imageUpload($request, 'assets/uploads');
        Gallery::create($input);
        $lists = Gallery::with('sub_category')->get();

        return view('admin.gallery.list',compact('lists'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Gallery::findorfail($id);
//         dd($post->sub_category->category);
        return view('admin.gallery.edit',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(Gallery $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        $input['slug'] = makeSlug($input['name'], new Gallery());
        if(!$request->image){
            $input['image'] = $request->old_image;
        }else{
            $input['image'] = imageUpload($request, 'assets/uploads', 'assets/uploads/'.$request->old_image);

        }

        if (!$request->edit_images){
            if ($request->images){
                if($files=$request->file('images')){
                    foreach($files as $k => $file){
                        $name = time() .$k. '.' . $file->getClientOriginalExtension();
                        $file->move('assets/uploads',$name);
                        $images[$k] = $name;
                    }
                    $input['images'] = implode(',',$images).','.$request->old_images;
                }

            }
        }else{
            if (isset($request->images)){
                if($files=$request->file('images')){
                    foreach($files as $k => $file){
                        $name = time() .$k. '.' . $file->getClientOriginalExtension();
                        $file->move('assets/uploads',$name);
                        $images[$k] = $name;
                    }
                    $input['images'] = implode(',',$images). ',' .$request->edit_images;
                    $input['images'] = substr_replace($input['images'], "", -1);

                }
//                $input['images'] += $input['edit_images'];
            }else{//if new_images->Has but not any new Images
                $input['images'] = $input['edit_images'];
                $input['images'] = substr_replace($input['images'], "", -1);
            }
        }
//        dd($input['images']);
        Gallery::findorfail($id)->update($input);

        $lists = Gallery::with('sub_category')->get();

        return view('admin.gallery.list',compact('lists'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gallery::destroy($id);
        return redirect()->back();
    }


}
