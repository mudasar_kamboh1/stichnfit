<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreOrder;
use App\Order;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Transformers\OrdersTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{


    /**
     * @var PostRepository
     */
    protected $orderRepository;
    protected $userRepository;

    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository){
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
//        $fractal = new Manager();
        $orders = Order::whereHas('user')->where('status', '!=' , 0)->orderBy('created_at', 'desc')->orderBy('updated_at', 'desc')->paginate(10);
//        $orders =  $fractal->createData(new Collection($orders, new OrdersTransformer()))->toArray();
        return view('admin.orders.list', compact('orders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrder $request)
    {

        try {
          
        }
        catch (\Exception $e) {
            dd($e);
            toastr()->error('Something went wrong..');
            return back();
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try {
            $order = $this->orderRepository->updateOrderDetails($request,$id);
            if($order){
                return response([
                    'status' => true,
                    'msg' => 'Order Details Updated Successfully.'
                ]);
            } else{
                return response([
                    'status' => false,
                    'msg' => 'Something went wrong.'
                ]);
            }
        }
        catch (\Exception $e) {
            dd($e);
            return response([
                'status' => false,
                'msg' => 'Something went wrong.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $order = Order::findOrFail($id);
        $order->delete();
        toastr()->success('SuccessFully Deleted!');
        return back();
    }

    public function getOrderDetails($id){
        $order = $this->orderRepository->findWhere(['id' => $id],['id']);
        if(!is_null($order)){
            $order = $this->orderRepository->modelObj()->with(['user', 'measurement'])->where('id',$id)->first();
            return view('admin.orders.order-detail-popup', compact('order'));
        }else{
            return '';
        }
    }
}
