<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StoreMeasurement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\MeasurementRepository;

class MeasurementController extends Controller
{


    protected $measurementRepository;

    public function __construct(MeasurementRepository $measurementRepository){
        $this->measurementRepository = $measurementRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMeasurement $request)
    {
        $measurement = $this->measurementRepository->create($request->all());
        if($measurement){
            return response([
                'status' => true,
                'msg' => 'Successfully saved!'
            ]);
        }else{
            return response([
                'status' => true,
                'msg' => 'Something went wrong!'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
