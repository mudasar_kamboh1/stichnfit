<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Category::all();
        $subCats = SubCategory::with('category')->get();
        return view('admin.category.subCat',compact('subCats','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $input['slug'] = makeSlug($input['name'], new SubCategory());
        SubCategory::create($input);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = SubCategory::findorfail($id);
        return view('admin.category.editSubCats',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        SubCategory::findorfail($id)->update($input);
//        dd('ok');
        $data = Category::all();
        $subCats = SubCategory::with('category')->get();
        return view('admin.category.subCat',compact('subCats','data'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubCategory::destroy($id);
        return back();
    }

    public function getCats($id)
    {
        $subCat = SubCategory::where('category_id', $id)->get();
        return response([
            'data' => $subCat
        ]);
    }
}
