<?php

namespace App\Http\Controllers;

use App\Jobs\sendEmails;
use App\Mail\OrderRecieved;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller {

    //

    public function __construct() {
        
    }

    /**
     * Redirect to the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendTrackOtp(Request $request)
    {

        if (Auth::check()) {
            return response([
                'status' => true,
                'redirect' => route('myOrders'),
                'msg' => 'Wait...'
            ]);
        } else {
            $request->validate([
                'mobile' => 'required|max:12|exists:users,mobile'
            ]);
            $user = User::whereMobile($request->mobile)->first();

            if (!is_null($user)) {
                if (!Auth::check()) {
                    $otp = uniqueOtp(rand(10000, 99999));
                    $message = 'Moaziz Saarif, Aap ka OTP password ' . $otp . ' hai. Is password ko enter krain aur apna amal mukammal krain.';
                    $smsSent = sendSms($user->mobile, $message);
                    if ($smsSent) {
                        $user->update(['otp' => $otp]);
                        if (Auth::check()) {
                            return response([
                                'status' => true,
                                'redirect' => route('myOrders'),
                                'msg' => 'Wait...'
                            ]);
                        } else {
                            return response([
                                'status' => true,
                                'redirect' => null,
                                'msg' => 'Moaziz Saarif, Aap ka login password aapke mobile number par bheja ja chuka hai.'
                            ]);
                        }
                    } else {
                        return response([
                            'status' => false,
                            'redirect' => null,
                            'msg' => 'Something went wrong!'
                        ]);
                    }
                }
            }
        }
    }

    /**
     * Redirect to the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkOrderTrakOtp(Request $request) {
        $request->validate([
            'otp' => 'required|exists:users,otp',
            'mobile' => 'required|max:12|exists:users,mobile'
        ]);

        $user = User::where(['otp' => $request->otp, 'mobile' => $request->mobile])->first();
        if (!is_null($user)) {
            $user->update(['otp' => null]);
            Auth::loginUsingId($user->id);
            if($request->order){
                $order = $user->orders()->where('status',0)->orderBy('created_at', 'desc')->first();
                if(!is_null($order)){
                    $order->update(['status' => 1]);
                    $message = "Moaziz Saarif, Stitch n Fit per order(".config('constants.order_token_prefix').$order->token.") karne ka shukria. Aap ki darkhwast per amal kia ja raha hai aur jald hi hamara numainda pickup time confirm karne k liye ap se rabta karega.";
                    sendSms($user->mobile, $message);
                    sendEmails::dispatch($order)->delay(now()->addSeconds(5));
                    return response([
                        'status' => true,
                        'redirect' => route('myOrders'),
                        'msg' => 'Moaziz Saarif, Stitch n Fit per order karne ka shukria. Aap ki darkhwast per amal kia ja raha hai aur jald hi hamara numainda pickup time confirm karne k liye ap se rabta karega.'
                    ]);
                }else{
                    return response([
                        'status' => false,
                        'msg' => 'Something is wrong with your Password!'
                    ]);
                }

            }else {
                return response([
                    'status' => true,
                    'redirect' => route('myOrders'),
                    'msg' => 'You have successfully logged in.'
                ]);
            }
        } else {
            return response([
                'status' => false,
                'msg' => 'Something is wrong with your Password!'
            ]);
        }
    }

}
