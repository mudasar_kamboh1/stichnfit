<?php

namespace App\Http\Controllers\Web;

use App\Http\Requests\SetSchedule;
use App\Http\Requests\StoreOrder;
use App\Jobs\sendEmails;
use App\Order;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;


class OrderController extends Controller
{


    /**
     * @var PostRepository
     */
    protected $orderRepository;
    protected $userRepository;

    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository){
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
       
        return view('web.order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrder $request)
    {

        try {
            $user = $this->orderRepository->saveOrder($request);
            if($user){
                return response([
                    'status' => true,
                    'msg' => "Moaziz Saarif, Aap ka login password aapke mobile number par bheja ja chuka hai."
                ]);
            }else{
                return response([
                    'status' => false,
                    'msg' => 'Something went wrong!'
                ]);
            }
        }
        catch (\Exception $e) {
            dd($e);
            return response([
                'status' => false,
                'msg' => 'Something went wrong!'
            ]);
        }
    }


    public function orderDetails($token)
    {
        try {
            $user = Auth::user();
            $order = $user->orders()->where('token', $token)->first();
            if (is_null($order)) {
                toastr()->error('Not Order Found!');
                return redirect()->route('myOrders');
            } else {
                return view('web.track-order', compact('order'));
            }
        }
        catch (\Exception $e) {
            toastr()->error('Something went wrong..');
            return back();
        }
    }
    

    public function thankYou(Request $request)
    {
        try {
            $user = Auth::user();
            $order = $user->orders()->orderBy('id', 'desc')->first();
            return view('web.order-submit', compact('order'));
        }
        catch (\Exception $e) {
            toastr()->error('Something went wrong..');
            return back();
        }
    }

    public function trackOrder(Request $request)
    {
        try {
            $user = Auth::user();
            $order = $user->orders()->with('measurement')->orderBy('id', 'desc')->first();
            if (!is_null($order)) {
                return view('web.track-order', compact('order'));
            } else {
                toastr()->error('Sorry, We not found your order.');
                return back();
            }
        }catch (\Exception $e) {
            toastr()->error('Something went wrong..');
            return back();
        }
    }
    
    public function myOrders(Request $request)
    {
        try {
            $user = Auth::user();
            $orders = $user->orders()->with('measurement')->where('status', '!=' , 0)->orderBy('created_at', 'desc')->orderBy('updated_at', 'desc')->paginate(10);
            toastr()->success('Successfully Retrieved!');
            return view('web.my-orders', compact('orders','user'));
        }
        catch (\Exception $e) {
            toastr()->error('Something went wrong..');
            return back();
        }
    }

    public function pickUpSchedule(SetSchedule $request)
    {
        try {

            $user = Auth::user();
            $order = $user->orders()->where('token',$request->token)->first();
            if(!is_null($order)){
                $today = now()->format('Y-m-d');
                $pickupTime = Carbon::parse($request->pickup_time)->format('Y-m-d');
                if($pickupTime != $today && $pickupTime > $today){
                    $update = $order->update([
                        'pickup_time' => Carbon::parse($request->pickup_time)->format('Y-m-d H:i:s'),
                        'area_id' => $request->area_id
                    ]);
                    if($update){
                        $message = "Moaziz Saarif, Aapke is order(".config('constants.order_token_prefix').$order->token.") ka pickup time ".Carbon::parse($request->pickup_time)->format('h:i A d-m-Y')." schedule kar dia gaya hai. Hamara derzi is time pe aapse rabta karlega";
                        sendSms($user->mobile, $message);
                        return response([
                            'status' => true,
                            'msg' => 'Time successfully updated!'
                        ]);
                    }else{
                        return response([
                            'status' => false,
                            'msg' => 'Something went wrong!'
                        ]);
                    }
                }else{
                    return response([
                        'status' => false,
                        'msg' => "Moaziz Saarif, Aap ki pick up time tabdeel karne ki darkhuwast par amal nahi kia jaskta."
                    ]);
                }

            }else{
                return response([
                    'status' => false,
                    'msg' => 'Something went wrong!'
                ]);
            }

        }
        catch (\Exception $e) {
            dd($e);
            return response([
                'status' => false,
                'msg' => 'Something went wrong!'
            ]);
        }
    }

    public function newOrder(){

        $user = Auth::user();
        $order['token'] = uniqueOrderToken(rand(100000, 999999));
        $order['status'] = 1;
        $order = Order::create($order);
        $order->user()->associate($user)->save();
        sendEmails::dispatch($order)->delay(now()->addSeconds(5));
        $message = "Moaziz Saarif, Stitch n Fit per order(".config('constants.order_token_prefix').$order->token.") karne ka shukria. Aap ki darkhwast per amal kia ja raha hai aur jald hi hamara numainda pickup time confirm karne k liye ap se rabta karega.";
        sendSms($user->mobile, $message);

        if($order){
            toastr()->success($message);
        }else{
            toastr()->error('Something went wrong..');
        }
        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $order = Order::find($id);
            $update = $order->update($request->all());
            if ($update) {
                toastr()->success('Successfully Updated!');
            } else {
                toastr()->error('Something went wrong..');
            }
            return back();
        }catch (\Exception $e) {
            dd($e);
            toastr()->error('Something went wrong..');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
