<?php

namespace App\Http\Controllers\Web;

use App\Gallery;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Gallery::whereHas('sub_category', function ($query) {
            $query->whereHas('category', function ($query) {
                $query->where('slug', 'blog');
            });
        })->orderBy('created_at','desc')->get();
        return view('web.blog-list', compact('posts'));
    }

    public function blogDetail($postSlug){

        $post = Gallery::where('slug', $postSlug)->whereHas('sub_category', function($query){
            $query->whereHas('category', function($query){
                $query->where('slug','blog');
            });
        })->orderBy('created_at','desc')->first();

        if(is_null($post)){
            toastr()->error('Blog Not Found!');
            return redirect('blog');
        }

        //Getting Last Blogs Added
        $posts = Gallery::where('slug','!=',$post->slug)->whereHas('sub_category', function ($query) {
            $query->whereHas('category', function ($query) {
                $query->where('slug', 'blog');
            });
        })->orderBy('created_at','desc')->limit(3)->get();
        return view('web.blog-detail', compact('posts','post'));
    }

    public function blogSubcategory($subCategorySlug){
        $posts = SubCategory::where('slug', $subCategorySlug)->first();
        if(is_null($posts)){
            toastr()->error('Blog Not Found!');
            return redirect('blog');
        }
        $posts = $posts->gallery;

        return view('web.blogSubcategoryList', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
