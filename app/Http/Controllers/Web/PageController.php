<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Gallery;
use App\SubCategory;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    //

    public function index(){
        $designs = Category::where('slug', '!=' ,'gallery')->where('slug', '!=','blog')->limit(3)->get();
        $posts = Gallery::whereHas('sub_category',function ($query){
            $query->where('slug','like', '%shirt%');
        })->orderBy('created_at','desc')->limit(6)->get();
        $posts1 = Gallery::whereHas('sub_category',function ($query){
            $query->where('slug','like', '%bottoms%');
        })->orderBy('created_at','desc')->limit(6)->get();

        $blogs = Gallery::whereHas('sub_category', function ($query) {
            $query->whereHas('category', function ($query) {
                $query->where('slug', 'blog');
            });
        })->orderBy('created_at','desc')->limit(2)->get();

        return view('web.web-home', compact('posts','posts1','designs','blogs'));
    }

    public function gallery(){
        $posts = Gallery::whereHas('sub_category', function ($query) {
            $query->whereHas('category', function ($query) {
                $query->where('slug', '!=', 'blog');
            });
        })->orderBy('created_at','desc')->get();
        return view('web.shirts', compact('posts'));
    }

    public function galleryDetail($subCategorySlug, $pstSlug){
        $post = Gallery::where('slug', $pstSlug)->whereHas('sub_category', function($query) use ($subCategorySlug){
            $query->where('slug',$subCategorySlug)->whereHas('category', function($query){
//                $query->where('slug','gallery');
            });
        })->orderBy('created_at','desc')->first();
        return view('web.single-gallery', compact('post'));
    }

    public function gallerySubcategory($subCategorySlug){
        $post = SubCategory::where('slug', $subCategorySlug)->with('gallery')->first();
        if(is_null($post)){
            toastr()->error('Gallery Not Found!');
            return redirect('gallery');
        }
        return view('web.galleryList', compact('post'));
    }

}
