<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreMeasurement extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->isAdmin()) {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chest' => 'required',
            'waist' => 'required',
            'kameez_length' => 'required',
            'arms_length' => 'required',
            'arms_width' => 'required',
            'trouser_length' => 'required',
            'shoulder' => 'required',
            'order_id' => 'required|exists:orders,id',
        ];
    }
}
