<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required',
            'address' => 'required'
        ];

    }

    protected function withValidator($validator)
    {

        if ($validator->fails()) {
            $messages = $validator->messages();

            foreach ($messages->all() as $message)
            {
                toastr()->error($message);
            }

            return $validator->errors()->all();
        }else{
            toastr()->success('SuccessFully Updated!');
        }

    }
}
