<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;


class StoreOrder extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:50',
            'mobile' => 'required|min:11|max:12',
            'email' => 'max:255',
            'address' => 'max:255',
            'type' => 'between:1,3',
        ];
    }

    protected function withValidator(Validator $validator)
    {
        if ($validator->fails()) {
            $messages = $validator->messages();

            foreach ($messages->all() as $message)
            {
                toastr()->error($message);
            }

            return $validator->errors()->all();
        }

    }

}
