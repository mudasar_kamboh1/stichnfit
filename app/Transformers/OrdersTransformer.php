<?php

namespace App\Transformers;

use App\Order;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class OrdersTransformer extends TransformerAbstract
{
    /**
     * @param  Model  $model
     * @return array
     */
    public function transform(Order $order)
    {
        return [
            'customer_name'		=> $order->user->name,
            'type'		=>  $order->type
        ];
    }
}
