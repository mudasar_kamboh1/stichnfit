<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table = 'sub_categories';

    protected $fillable = [
        'name',
        'category_id',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function gallery()
    {
        return $this->hasMany('App\Gallery','subCategory_id');
    }
}
