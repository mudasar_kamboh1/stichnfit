<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    //
    use SoftDeletes;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    public function measurement()
    {
        return $this->hasOne('App\Measurement');
    }

    public function area()
    {
        return $this->belongsTo('App\Area');
    }
}
