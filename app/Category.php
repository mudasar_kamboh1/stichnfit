<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    protected $fillable = [
        'name',
        'type',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];
    public function subcategories()
    {
        return $this->hasMany('App\SubCategory','category_id');
    }

}
