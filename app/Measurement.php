<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    //

    protected $fillable = [
        'order_id','user_id','shirt_length','shoulder','arm_length','arm_width','wrist_style','wrist_width','chest',
        'shirt_waist','hip','ghera','trouser_length','thai','knee','trouser_style','trouser_waist','belt',
        'half_belt','half_elastic','side_zip','front_zip','alround_elastic','front_plane_round_elastic','image'
    ];
    
    protected $guarded = [];

    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
