<?php
namespace App\Repositories;

use App\User;
use App\Measurement;
use Carbon\Carbon;
use jeremykenedy\LaravelRoles\Models\Role;
use Prettus\Repository\Eloquent\BaseRepository;

class OrderRepository extends BaseRepository {

/**
* Specify Model class name
*
* @return string
*/
    function model() {
        return "App\Order";
    }

    function modelObj() {
        return $this->model;
    }


    public function saveOrder($request){
        // Posting Data
        $input = $request->only('name','email','mobile');

        $user = User::whereMobile($request->mobile)->first();
        if(is_null($user)){
            $user = User::create($input);
        }

        $role = Role::whereSlug('user')->first();
        if(is_null($role)){
            $user->attachRole($role);
        }

        $order = $request->only('type');
        $order['token'] = uniqueOrderToken(rand(100000, 999999));
        $order['status'] = 0;
        $order = $this->create($order);
        $order->user()->associate($user)->save();

        $otp = uniqueOtp(rand(100000, 999999));
        $user->update(['otp' => $otp]);
//        $message = 'Your OTP(One Time Password) code is '.$otp.'. Please enter and continue!';
        $message = 'Moaziz Saarif, Aap ka OTP password '.$otp.' hai. Is password ko enter krain aur apna order mukammal krain.';
        sendSms($user->mobile, $message);
        return $user;
    }
    
    public function updateOrderDetails($request, $orderId){

        $orderObj = $this->find($orderId);
        $orderStatus = $orderObj->status;
        $status = (int)$request->status;

        $user = $request->only('address');
        $order = $request->only('type','status','description','title');
        if($request->pickup_time){
            $order['pickup_time'] = Carbon::parse($request->pickup_time)->format('Y-m-d H:i:s');
        }

//        $request->validate([
//            'image' => 'required|file|image|mimes:jpeg,png,gif,webp|max:2048'
//        ]);

        if ($request->hasFile('image')){
            $order['image'] = imageUpload($request, 'assets/uploads/orders');
        }

        $orderObj->user()->update($user);
        $orderObj->update($order);

        $message = '';

        if($status == 1 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, We have received your order '.config('constants.order_token_prefix').$orderObj->token.' and it is pending pickup. Our team will reach out to you to get the material. ';
        }elseif ($status == 2 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, It is to inform you that your order '.config('constants.order_token_prefix').$orderObj->token.' has been collected by our team and forwarded to the stitching department. ';
        }
        elseif($status == 3 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, You will be pleased to know that your order '.config('constants.order_token_prefix').$orderObj->token.' is in the stitching phase. We will inform you once it is completed.';
        }
        elseif($status == 4 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, We are pleased to inform you that your order '.config('constants.order_token_prefix').$orderObj->token.' has been stitched and will be delivered to you as soon as possible.';
        }
        elseif($status == 5 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, Our rider is on his way to deliver your dress to you. We hope you will like it.';
        }
        elseif($status == 6 && $status != $orderStatus && $status > $orderStatus){
            $message = 'Dear Customer, Since you have received your order '.config('constants.order_token_prefix').$orderObj->token.', we really look forward to knowing your feedback. Share your experience with us and let us know how do you find your dress. ';
        }


        if($message != ''){
            sendSms($orderObj->user->mobile, $message);
        }

        if(!is_null($orderObj->measurement)){
            $orderObj->measurement()->update($request->except('_token','_method','address','status','type','order_id','description','title','image','pickup_time','notes'));
        }else{
            $measurement = Measurement::create($request->except('_token','_method','address','status','type','description','title','image','pickup_time','notes'));
            $measurement->user()->associate($orderObj->user)->save();
        }
        return true;
    }
}
