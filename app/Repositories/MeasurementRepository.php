<?php
namespace App\Repositories;

use App\Measurement;
use Prettus\Repository\Eloquent\BaseRepository;

class MeasurementRepository extends BaseRepository {

/**
* Specify Model class name
*
* @return string
*/
    function model() {
        return "App\Measurement";
    }

    function modelObj() {
        return $this->model;
    }
}
