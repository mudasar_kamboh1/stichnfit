<?php

namespace App;

use App\User;
use Illuminate\Support\Facades\Auth;

class Reports
{
    private $totalUsers;
    private $totalOrders;
    private $newOrders;
    private $cancelOrders;

    /**
     * @return integer
     */
    public function getTotalUsers()
    {
        if (is_null($this->totalUsers)) {
            $this->totalUsers = User::count();
        }

        return $this->totalUsers;
    }

    public function getTotalOrders()
    {
        if (is_null($this->totalOrders)) {
            $order = Order::where('status','>=',1)->get();
            $this->totalOrders = $order->count();
        }

        return $this->totalOrders;
    }

    public function getNewOrders()
    {
        if (is_null($this->newOrders)) {
            $order = Order::where('status',1)->get();
            $this->newOrders = $order->count();
        }

        return $this->newOrders;
    }

    public function getCancelOrders()
    {
        if (is_null($this->cancelOrders)) {
            $order = Order::where('status',8)->get();
            $this->cancelOrders = $order->count();
        }

        return $this->cancelOrders;
    }
}
