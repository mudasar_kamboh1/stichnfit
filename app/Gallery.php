<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';

    protected $fillable = [
        'name',
        'image',
        'images',
        'description',
        'subCategory_id',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public function sub_category()
    {
        return $this->belongsTo('App\SubCategory', 'subCategory_id');
    }
}
