<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Custom Constants
    |--------------------------------------------------------------------------
    |
    */

    'order_token_prefix' => 'SAF',

    'order_types' => [
        1 => 'Visit Tailor',
        2 => 'Old Measurements',
        3 => 'New Measurements',
    ],

    'order_status' => [
        1 => 'Pending',
        2 => 'Pick Up',
        3 => 'In Stitching',
        4 => 'Completed',
        5 => 'In Delivery',
        6 => 'Delivered',
        7 => 'Delivery Rejected',
        8 => 'Cancelled By User',
        9 => 'Reject By Admin',
    ],

    'order_colors' => [
        1 => '#990099',
        2 => '#660099',
        3 => '#00b8ff',
        4 => '#ba5f8f',
        5 => '#000ec9',
        6 => '#8fba5f',
        7 => '#ff0011',
        8 => '#ff0000',
        9 => '#e50202',
    ]
    
    


];
