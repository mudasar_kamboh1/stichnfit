$(window).on('load', function()
{
    var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
    $('#textbox').inputmask({
        mask: phones,
        greedy: false,
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
    $('#textbox2').inputmask({
        mask: phones,
        greedy: false,
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
    $('#track-mobile').inputmask({
        mask: phones,
        greedy: false,
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
});

var mobile;
$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:3,
        itemsDesktop:[1000,3],
        itemsDesktopSmall:[990,2],
        itemsTablet:[767,1],
        pagination:true,
        navigation:true,
        autoPlay:true
    });
});
function getId(str)
{
    return str.replace(/[^0-9.]/g,'');
}

$("#order-form").validate({
    rules: {
        name:{
            required: true,
        },

        mobile: {
            required: true,


        },

        email: {
            email: true
        },
        address: {
            required: true,

        },
        //
        // shoulder: "required",
        // chest:"required",
        // waist:"required",
        // kameez_length: "required",
        // trouser_length: "required",
        // arms_length: "required",
        // arms_width: "required",
        // radioGroup:"required",
    },
    messages: {
        name:
            {required:"Please enter your name",
                pattern: "Please enter valid name",
            },

        mobile: {
            required: "Please enter a valid number",
            maxlength: "Mobile number consist of numeric characters",

        },

        email: "Please enter a valid email address",
        // radioGroup: "Please select one of these",
        //
        // shoulder: "Please enter your shoulder size",
        // chest: "Please enter your chest size",
        // waist: "Please enter your waist size",
        // kameez_length: "Please enter your kameez length",
        // trouser_length: "Please enter your trouser length",
        // arms_length: "Please enter your arms length",
        // arms_width: "Please enter your width",



    }

});

$('.order-detail').on('click',function () {
    getData($(this).data('order-id'));
});

$(document).on('submit','#order-form',function (e) {
    e.preventDefault();
    let $this = $(this);
    $.ajax({
        url: "{{url('order')}}",
        type: "POST",
        data : $this.serialize(),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(result){
            if(result.status){
                swal({
                    title: 'Success',
                    text: result.msg ,
                    icon: "success",
                    button: "OK"
                });
                location.href = "{{route('thankyou')}}";
            }else{
                swal({
                    title: 'Error',
                    text: result.msg ,
                    icon: "error",
                    button: "OK"
                });
            }
        },
        error: function (request, status, error) {
            let json = $.parseJSON(request.responseText);
            $.each(json.errors, function(key, value){
                toastr.error(value);
            });
        }
    });
});

$(document).on('submit','#send-track-otp',function (e) {
    e.preventDefault();
    $('#otp-mobile').val($('#mobile-number3').val());
    $('#popup-type').val('track');
    let $this = $(this);
    sendOtp($this);
});

$(document).on('submit','#send-order-otp',function (e) {
    e.preventDefault();
    $('#otp-mobile').val($('#mobile-number2').val());
    $('#popup-type').val('my-orders');
    let $this = $(this);
    sendOtp($this);
});




function sendOtp($this){
    $.ajax({
        url: "{{url('send-track-otp')}}",
        type: "GET",
        data : $this.serialize(),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(result){
            if(result.status){
                if(result.redirect !== null){
                    toastr.info('Getting details...');
                    location.href = result.redirect;
                }else{
                    toastr.success(result.msg);
                    $('#my-orders').modal('hide');
                    $('#track').modal('show');
                }
            }else{
                swal({
                    title: 'Error',
                    text: result.msg ,
                    icon: "error",
                    button: "OK"
                });
            }
        },
        error: function (request, status, error) {
            let json = $.parseJSON(request.responseText);
            $.each(json.errors, function(key, value){
                toastr.error(value);
            });
        }
    });
}

$(document).on('submit','#check-track-otp',function (e) {
    e.preventDefault();
    let $this = $(this);
    $.ajax({
        url: "{{url('check-track-otp')}}",
        type: "GET",
        data : $this.serialize()+ '&type=' + $('#popup-type').val(),
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        success: function(result){
            if(result.status){
                swal({
                    title: 'Success',
                    text: result.msg ,
                    icon: "success",
                    button: "OK"
                });
                $('#track').modal('hide');
                setTimeout(function () {
                    location.href = result.redirect;
                },1000);
            }else{
                swal({
                    title: 'Error',
                    text: result.msg ,
                    icon: "error",
                    button: "OK"
                });
            }
        },
        error: function (request, status, error) {
            let json = $.parseJSON(request.responseText);
            $.each(json.errors, function(key, value){
                toastr.error(value);
            });
        }
    });
});


