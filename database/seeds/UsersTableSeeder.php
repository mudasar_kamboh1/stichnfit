<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin',
                'email' => 'admin@stitchnfit.com',
                'password' => Hash::make('secret123!@#'),
                'is_admin' => 1,
                'logo_number' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Manager',
                'email' => 'manager@stitchnfit.com',
                'password' => Hash::make('managers123!@#'),
                'is_admin' => 1,
                'logo_number' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'name' => 'Agent',
                'email' => 'agent@stitchnfit.com',
                'password' => Hash::make('agents123!@#'),
                'is_admin' => 1,
                'logo_number' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]

        ]);
    }
}
