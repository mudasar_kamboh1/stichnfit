<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableMeasurements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->char('shoulder',50)->nullable();
            $table->char('chest',50)->nullable();
            $table->char('shirt_length',50)->nullable();
            $table->char('shirt_waist',50)->nullable();
            $table->char('arm_length',50)->nullable();
            $table->char('arm_width',50)->nullable();
            $table->char('wrist_style',50)->nullable();
            $table->char('wrist_width',50)->nullable();
            $table->char('hip',50)->nullable();
            $table->char('ghera',50)->nullable();
            $table->char('trouser_length',50)->nullable();
            $table->char('trouser_waist',50)->nullable();
            $table->char('thai',50)->nullable();
            $table->char('knee',50)->nullable();
            $table->char('trouser_style',50)->nullable();
            $table->boolean('belt')->default(0);
            $table->boolean('half_belt')->default(0);
            $table->boolean('half_elastic')->default(0);
            $table->boolean('side_zip')->default(0);
            $table->boolean('front_zip')->default(0);
            $table->boolean('alround_elastic')->default(0);
            $table->boolean('front_plane_round_elastic')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
