<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('area_id')->after('id')->nullable();
            $table->string('title')->after('token')->nullable();
            $table->string('image')->after('title')->nullable();
            $table->longText('description')->after('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('area_id');
            $table->dropColumn('title');
            $table->dropColumn('image');
            $table->dropColumn('description');
        });
    }
}
