<form id="update-order-details" method="POST" action="{{route('admin::orders.update', $order->id)}}" enctype="multipart/form-data">
      {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="new-measurements">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Token:</label>
                    <input class="form-control" value="{{ !is_null($order)? config('constants.order_token_prefix').$order->token : 'Not Available'}}" readonly/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input class="form-control" value="{{ !is_null($order->user) && $order->user->name? $order->user->name : 'Not Available'}}" readonly/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Email:</label>
                    <input class="form-control" value="{{ !is_null($order->user) && $order->user->email ? $order->user->email : 'Not Available'}}" readonly/>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Verified Mobile:</label>
                    <input class="form-control" value="{{ !is_null($order->user)? $order->user->mobile : 'Not Available'}}" readonly/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Additional Mobile:</label>
                    <input class="form-control" value="{{ !is_null($order) && $order->additional_phone ? $order->additional_phone : '-- -- --' }}" readonly/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Address:</label>
                    <input class="form-control" name="address" value="{{ !is_null($order) ? $order->user->address : '' }}" />
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Pickup Time:</label>
                    <input name="pickup_time" class="form-control"  value="{{ !is_null($order) && $order->pickup_time ? \Carbon\Carbon::parse($order->pickup_time)->format('Y-m-d h:i A') : ''}}"  />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Type:</label>
                    <span class="cstm-select">
                <select class="form-control" name="type" required>
                    <option value="">Select Type</option>
                    @foreach(config('constants.order_types') as $k =>  $status)
                        <option value="{{$k}}" {{ (!is_null($order) && $order->type == $k) ? 'selected' : '' }}>{{$status}}</option>
                    @endforeach
                </select>
            </span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">Status:</label>
                    <span class="cstm-select">
                <select class="form-control" name="status" required>
                    <option value="">Select Status</option>
                    @foreach(config('constants.order_status') as $k =>  $status)
                        <option value="{{$k}}" {{ (!is_null($order) && $order->status == $k) ? 'selected' : '' }}>{{$status}}</option>
                    @endforeach
                </select>
            </span>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Order Note:</label>
                    <textarea name="notes" maxlength="250" class="form-control" rows="5">{{ !is_null($order) && $order->notes ? $order->notes : '-- -- --'}}</textarea>
                </div>
            </div>


        </div>
    </div>



{{--<div class="row padd-10">--}}
    {{--<div class="col-md-2" style="padding-right: 0px;">--}}
        {{--<div class="form-group">--}}
            {{--<label for="contact" style="width:100%;">Address:</label>--}}

        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-10" style="padding-left: 3px;">--}}
        {{--<div class="form-group">--}}
            {{--<textarea name="address" maxlength="250" class="form-control" rows="5">{{ !is_null($order) ? $order->description : '' }}</textarea>--}}

            {{--<input type="text" maxlength="250" class="form-control cstm-input" name="address" value="{{ !is_null($order->user) && !empty($order->user->address)? $order->user->address : ''}}" placeholder="Add Address"/></span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

{{--<div class="row padd-10">--}}
    {{--<div class="col-md-6" style="padding-left: 3px;">--}}
        {{--<div class="form-group">--}}
            {{--<label for="contact">Order Note:</label><span>{{ !is_null($order) && $order->notes ? $order->notes : '-- -- --'}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="col-md-6" style="padding-left: 3px;">--}}
        {{--<div class="form-group">--}}
            {{--<label for="contact">Pickup Time:</label><span>{{ !is_null($order) && $order->pickup_time ? \Carbon\Carbon::parse($order->pickup_time)->format('Y-m-d h:i A') : '-- -- --'}}</span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
    {{--<div class="col-md-6">--}}
        {{--<div class="form-group">--}}
            {{--<label for="contact">Type:</label>&nbsp;--}}
            {{--<span class="cstm-select">--}}
                {{--<select class="form-control" name="type" required>--}}
                    {{--<option value="">Select Type</option>--}}
                    {{--@foreach(config('constants.order_types') as $k =>  $status)--}}
                        {{--<option value="{{$k}}" {{ (!is_null($order) && $order->type == $k) ? 'selected' : '' }}>{{$status}}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</span>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-md-6">--}}
        {{--<div class="form-group">--}}
            {{--<label for="contact">Status:</label>&nbsp;--}}
            {{--<span class="cstm-select">--}}
                {{--<select class="form-control" name="status" required>--}}
                    {{--<option value="">Select Status</option>--}}
                    {{--@foreach(config('constants.order_status') as $k =>  $status)--}}
                        {{--<option value="{{$k}}" {{ (!is_null($order) && $order->status == $k) ? 'selected' : '' }}>{{$status}}</option>--}}
                    {{--@endforeach--}}
                {{--</select>--}}
            {{--</span>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="measurement-section">
<div class="new-measurements" id="shirt-measurements">
    <h2>Add Shirt Measurements</h2>

        <input type="hidden" name="order_id" value="{{$order->id}}">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Shirt length:</label>
                <input type="text" placeholder="Shirt length" maxlength="30" class="form-control cstm-control" name="shirt_length" value="{{ !is_null($order->measurement) ? $order->measurement->shirt_length : ''}}" >
            </div>
        </div>
                <div class="col-md-3">
            <div class="form-group">
                <label for="contact">Shoulder:</label>
                <input type="text" placeholder="Shoulder" class="form-control cstm-control" maxlength="30" name="shoulder" value="{{ !is_null($order->measurement) ? $order->measurement->shoulder : ''}}" >

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Arms length:</label>
                <input type="text" placeholder="Arms length" class="form-control cstm-control"maxlength="30"  name="arm_length" value="{{ !is_null($order->measurement) ? $order->measurement->arm_length : ''}}" >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Arms width:</label>
                <input type="text"  placeholder="Arms width" class="form-control cstm-control" maxlength="30" name="arm_width" value="{{ !is_null($order->measurement) ? $order->measurement->arm_width : ''}}" >
            </div>
        </div>


    </div>



        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Kalai Style:</label>
                    <input type="text" placeholder="Kalai Style" class="form-control cstm-control" maxlength="30" name="wrist_style" value="{{ !is_null($order->measurement) ? $order->measurement->wrist_style : ''}}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Kalai Width:</label>
                    <input type="text"  placeholder="Kalai Width" class="form-control cstm-control" maxlength="30" name="wrist_width" value="{{ !is_null($order->measurement) ? $order->measurement->wrist_width : ''}}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Chest:</label>
                    <input type="text" placeholder="Chest" class="form-control cstm-control" maxlength="30" name="chest" value="{{ !is_null($order->measurement) ? $order->measurement->chest : ''}}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Shirt Waist:</label>
                    <input type="text" placeholder="Shirt Waist" class="form-control cstm-control" maxlength="30" name="shirt_waist" value="{{ !is_null($order->measurement) ? $order->measurement->shirt_waist : ''}}" >
                </div>
            </div>

        </div>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="contact">Hip:</label>
                <input type="text" placeholder="Hip" class="form-control cstm-control" maxlength="30" name="hip" value="{{ !is_null($order->measurement) ? $order->measurement->hip : ''}}" >
            </div>
        </div>

         <div class="col-md-3">
          <div class="form-group">
            <label for="name">Ghera:</label>
            <input type="text"  placeholder="Ghera" class="form-control cstm-control" maxlength="30" name="ghera" value="{{ !is_null($order->measurement) ? $order->measurement->ghera : ''}}" >
          </div>
        </div>
    </div>


</div>
<div class="new-measurements" id="trouser-measurements">
    <h2>Add Trouser Measurements</h2>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Trouser length:</label>
                <input type="text" placeholder="Trouser length" class="form-control cstm-control" maxlength="30" name="trouser_length" value="{{ !is_null($order->measurement) ? $order->measurement->trouser_length : ''}}" >
            </div>
        </div>
                <div class="col-md-3">
            <div class="form-group">
                <label for="contact">Thai:</label>
                <input type="text" placeholder="Thai" class="form-control cstm-control" maxlength="30" name="thai" value="{{ !is_null($order->measurement) ? $order->measurement->thai : ''}}" >

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Gutna:</label>
                <input type="text" placeholder="Gutna" class="form-control cstm-control" maxlength="30" name="knee" value="{{ !is_null($order->measurement) ? $order->measurement->knee : ''}}" >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Trouser Style:</label>
                <input type="text"  placeholder="Trouser Style" class="form-control cstm-control" maxlength="30" name="trouser_style" value="{{ !is_null($order->measurement) ? $order->measurement->trouser_style : ''}}" >
            </div>
        </div>


    </div>

    <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Trouser Waist:</label>
                    <input type="text" placeholder="Trouser Waist" class="form-control cstm-control" maxlength="30" name="trouser_waist" value="{{ !is_null($order->measurement) ? $order->measurement->trouser_waist : ''}}" >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Belt:</label>
                    <input type="checkbox" class="option-input radio"  name="belt" value="1" {{ (!is_null($order->measurement) && $order->measurement->belt == 1)? 'checked' : ''}} >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Half Belt:</label>
                    <input type="checkbox" class="option-input radio"  name="half_belt" value="1" {{ (!is_null($order->measurement) && $order->measurement->half_belt == 1)? 'checked' : ''}} >
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="name">Half Elastic:</label>
                    <input type="checkbox" class="option-input radio" name="half_elastic" value="1" {{ (!is_null($order->measurement) && $order->measurement->half_elastic == 1)? 'checked' : ''}} >
                </div>
            </div>

        </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Side Zip:</label>
                <input type="checkbox" class="option-input radio" name="side_zip" value="1" {{ (!is_null($order->measurement) && $order->measurement->side_zip == 1)? 'checked' : ''}} >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Front Zip:</label>
                <input type="checkbox" class="option-input radio" name="front_zip" value="1" {{ (!is_null($order->measurement) && $order->measurement->front_zip == 1)? 'checked' : ''}} >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Alround Elastic:</label>
                <input type="checkbox" class="option-input radio" name="alround_elastic" value="1" {{ (!is_null($order->measurement) && $order->measurement->alround_elastic == 1)? 'checked' : ''}} >
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="name">Front plane Round Elastic:</label>
                <input type="checkbox" class="option-input radio" name="front_plane_round_elastic" value="1" {{ (!is_null($order->measurement) && $order->measurement->front_plane_round_elastic == 1)? 'checked' : ''}}  />
            </div>
        </div>
    </div>

</div>

    <div class="new-measurements">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Order Title:</label>
                    <input name="title" class="form-control" value="{{ !is_null($order) ? $order->title : '' }}" />
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">More Details:</label>
                    <textarea name="description" class="form-control" rows="5">{{ !is_null($order) ? $order->description : '' }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="new-measurements">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                        <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="70" height="70">
                    @else
                        <img src="{{asset('assets/img/download.jpg')}}" alt="Order Image" title="" width="70" height="70">
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Image:</label>
                    <input type="file" name="image" class="form-control" />
                </div>
            </div>
        </div>

    </div>
        <div class="text-left">
            <button type="submit" class="btn btn-default new-measurement-submit">Save</button>
            <button type="submit" class="btn btn-default new-measurement-submit" style="background-color: #f7f7f7;color: black;padding: 2px 30px;" data-dismiss="modal" aria-label="Close">Cancel</button>
        </div>
  
    
</div>
 </form>