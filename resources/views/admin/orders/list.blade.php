{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
{!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
@parent
@endsection

@section('content')
<div style="background-color: #fff; padding: 15px 15px 0">

    <div class="row">
        <div class="col-md-4">
            {{--<span>Search Order</span>--}}
            {{--<input class="form-control" id="myInput" type="text" placeholder="Search By Name, Email And Token ">--}}
        </div>
        {{--<div class="col-md-4">--}}
            {{--<span>Search by Token</span>--}}
            {{--<input class="form-control" id="myInput1" type="text" placeholder="Search..">--}}
        {{--</div>--}}
        {{--<div class="col-md-4">--}}
            {{--<span>Search by Type</span>--}}
            {{--<input class="form-control" id="myInput2" type="text" placeholder="Search..">--}}
        {{--</div>--}}
    </div>
    <br>
    <div class="table-responsive list-records">
        <table class="table table-hover table-bordered">
            <thead>
            <!--<th style="width: 10px;"><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button></th>-->
            <th>Token No.</th>
            <th>Image</th>
            <th>Name</th>
            <th>Mobile</th>
            <th>Email</th>
            <th>Type</th>
            <th>Status</th>
            {{--<th>Status</th>--}}
            <th style="width: 120px;">Actions</th>
            </thead>
            <tbody id="myTable">
            @foreach ($orders as $order)
                <tr data-id="{{$order->id}}">
                    <td>{{ config('constants.order_token_prefix').'-'.$order->token }}</td>
                    <td>
                        @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                            <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="70" height="70">
                        @else
                            <img src="{{asset('/assets/img/download.jpg')}}" alt="Order Image" title="" width="70" height="70">
                        @endif
                    </td>
                    <td>{{ !is_null($order->user)? $order->user->name:'' }}</td>
                    <td><a href="tel:{{ !is_null($order->user)? $order->user->mobile:'' }}" >{{ !is_null($order->user)? $order->user->mobile:'' }}</a></td>
                    <td>{{ !is_null($order->user)? $order->user->email ? $order->user->email : 'N/A' : '' }}</td>
                    <td>{{ config('constants.order_types.'.$order->type) ? config('constants.order_types.'.$order->type) : 'N/A' }}</td>
                    <td><label class="status" style="background-color: {{ config('constants.order_colors.'.$order->status) }}">{{ config('constants.order_status.'.$order->status) }}</label></td>
                    <td>
                        <div class="btn-group">
                            <button class="btn btn-primary btn-sm order-detail"
                               data-order-id="{{$order->id}}" data-toggle="modal" data-target="#view-detail"><i class="fa fa-info"></i> </button>
                            <form action="{{route('admin::orders.destroy', $order->id)}}" method="POST" style="float: left;margin-left: 10px;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></button>
                            </form>
                        </div>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
</div>

<div class="pull-right">

    <br>
    {{$orders->render()}}
</div>
<!-- The Modal -->

<div class="modal fade" id="view-detail">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal body -->
            <div class="modal-body">
                <div class="container cstm-container">
                    <div class="col-md-12 submit-order-detail">

                    </div>

                </div>
            </div>
            <!-- Modal footer -->
    </div>
</div>
@endsection

@section('footer-extras')

    <script type="application/javascript">
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });
        });

        $('.order-detail').on('click',function () {
            getData($(this).data('order-id'));
        });

        $(document).on('submit','.new-measurements-form',function (e) {
            e.preventDefault();
            $('.loader').show();
            let $this = $(this);
            $.ajax({
                url: "{{route('admin::measurement.store')}}",
                type: "POST",
                data : $this.serialize(),
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    $('.loader').hide();
                    if(result.status){
                        swal({
                            title: 'Success',
                            text: result.msg ,
                            icon: "success",
                            button: "OK",
                        });
                        getData($('input[name=order_id]').val());
                    }else{
                        swal({
                            title: 'Error',
                            text: result.msg ,
                            icon: "error",
                            button: "OK",
                        });
                    }
                }
            });
        });

        function getData(id) {
            $('.loader').show();
            $('.submit-order-detail').html('');
            $.ajax({
                url: "{{url('admin/orders/details')}}/"+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    $('.loader').hide();
                    $('.submit-order-detail').html(result);
                }
            });
        }
        
        
        $(document).on('submit','#update-order-details',function (e) {
            e.preventDefault();
            $('.loader').show();
            let $this = $(this);
            var formData = new FormData(this);
            $.ajax({
                url: $this.attr('action'),
                type: "POST",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    $('.loader').hide();
                    if(result.status){
                        $('#view-detail').modal('hide');

                        swal({
                            title: 'Success',
                            text: result.msg ,
                            icon: "success",
                            button: "OK",
                        });
                        location.reload();
//                       getData($('input[name=order_id]').val());
                    }else{
                        swal({
                            title: 'Error',
                            text: result.msg ,
                            icon: "error",
                            button: "OK",
                        });
                    }
                },
                error: function (request, status, error) {
                    $('.loader').hide();
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.warning(value);
                    });
                }
            });
        });
    </script>
@endsection
