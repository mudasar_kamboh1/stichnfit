{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Category')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div style="background-color: #fff; padding: 15px 15px 0" xmlns="http://www.w3.org/1999/html">

        <div class="row">
            <div class="col-md-4">
                <div class="box-tools">
                    <h3>Edit Category</h3>
                </div>
            </div>

        </div>

        <br>
        <div class="table-responsive list-records">
            <form class="form" role="form" method="POST" action="{{ route('admin::category.update',$data->id) }}">
                @csrf
                    {{ method_field('PUT') }}
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-group" >
                            <label for="usr">Category:</label>
                            <input value="{{ $data->name }}" type="text" name="name" class="form-control" placeholder="Add New Category" required>
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Title:</label>
                            <input  value="{{ $data->meta_title }}" type="text" class="form-control" id="meta_title" name="meta_title" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Description:</label>
                            <input  value="{{ $data->meta_description }}" type="text" class="form-control" id="meta_description" name="meta_description" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta keywords:</label>
                            <input value="{{ $data->meta_keywords }}" type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
  @endsection
  @section('footer-extras')

  @endsection
