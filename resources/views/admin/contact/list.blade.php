{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div style="background-color: #fff; padding: 15px 15px 0" xmlns="http://www.w3.org/1999/html">


        <div class="table-responsive list-records">
            <table class="table table-hover table-bordered">
                <thead>
                <tr><th>#</th>
                    <th>Name</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th style="width: 120px;">Actions</th>
                </tr></thead>
                <tbody>
                @php
                    $x=1;
                @endphp
                @foreach($requests as $request)
                    @php
                        $x=1;
                        $x++;
                    @endphp
                    <tr>
                        <td>{{ $x }}</td>
                        <td>{{ $request->name }}</td>
                        <td>{{ $request->mobile }}</td>
                        <td>{{ $request->email }}</td>
                        <td>{{ $request->message }}</td>
                        <td>
                            <form action="{{ route('admin::contacts.destroy',$request->id) }}" method="post">
                                @csrf
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger" id="deleteRequest"> Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
            @endsection

@section('footer-extras')

@endsection
