{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Admin')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $reports->getTotalUsers() }}</h3>
                    <p>Users</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::users.index') }}" class="small-box-footer">List Users <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-success">
                <div class="inner">
                    <h3>{{ $reports->getTotalOrders() }}</h3>
                    <p>Total Orders</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-sun-o" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::orders.index') }}" class="small-box-footer">List Orders <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-info">
                <div class="inner">
                    <h3>{{ $reports->getNewOrders() }}</h3>
                    <p>New Orders</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::orders.index') }}" class="small-box-footer">List Orders <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-warning">
                <div class="inner">
                    <h3>{{ $reports->getCancelOrders() }}</h3>
                    <p>Cancel Orders</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-ban" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::orders.index') }}" class="small-box-footer">List Orders <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection
