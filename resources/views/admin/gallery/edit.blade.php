{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div style="background-color: #fff; padding: 15px 15px 0">

        <div class="row">
            <div class="col-md-4">
            </div>
        </div>
        <br>
        <div class="table-responsive list-records">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Gallery Data</h4>
                </div>
                <form action="{{ route('admin::gallery.update',$post->id) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        category :<select name="category" id="category" class="form-control">
                            <option>Select</option>

                            @foreach(App\Category::all() as $options)
                                <option value="{{ $options->id }}"
                                        {{ (!is_null($post->sub_category) && !is_null($post->sub_category->category)) ? $post->sub_category->category->id == $options->id ? 'selected' : '' : ''}}>{{ $options->name }}</option>
                            @endforeach
                        </select>
                        subcategory :
                        <select name="subCategory_id" id="subcategory" class="form-control">
                            @foreach(\App\SubCategory::all() as $options1)
                                <option value="{{ $options1->id }}"
                                        {{ !is_null($post->sub_category) ? $post->sub_category->id == $options1->id ? 'selected' : '' : ''}}>{{ $options1->name }}</option>
                            @endforeach
                        </select>
                        Title :<input type="text" name="name" class="form-control" value="{{ $post->name }}">
                        <input type="hidden" name="old_image" id="old_image" value="{{$post->image}}" />
                        <img src="{{asset('assets/uploads')}}{{ '/'.$post->image }}" style="width: 70px;height: 70px">

                        <br>
                            Image :<input type="file" name="image" class="form-control">
                        <br>
                        @php
                            $post->images = explode(',',$post->images);
                        @endphp
                        Images :
                        @foreach($post->images as $image)
                            <div id="{{$image}}" style="display:inline">
                                <img src="{{asset('assets/uploads')}}{{ '/'.$image }}" class="images" data-src="{{$image}}" width="50px" height="50px">
                                <span style="color: red" onclick="removeImage($(this))" class="glyphicon glyphicon-remove"></span>
                            </div>
                        @endforeach
                        <input type="file" class="form-control" id="images" name="images[]" placeholder="address" multiple="">
                        <input type="hidden" id="new_image" name="edit_images" />
                        @php
                            $post->images = implode(',',$post->images);
                        @endphp
                        <input type="hidden" name="old_images" value="{{$post->images}}" />
                        <br>
                        Description :<textarea id="textarea" type="text" name="description" class="form-control" >{{ $post->description }}</textarea>

                        <div class="form-group">
                            <label for="usr">Meta Title:</label>
                            <input  value="{{ $post->meta_title }}" type="text" class="form-control" id="meta_title" name="meta_title" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta Description:</label>
                            <input  value="{{ $post->meta_description }}" type="text" class="form-control" id="meta_description" name="meta_description" />
                        </div>
                        <div class="form-group">
                            <label for="usr">Meta keywords:</label>
                            <input value="{{ $post->meta_keywords }}" type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                        </div>
                    </div>

                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Update">

                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('footer-extras')

    <script type="application/javascript">


        $(document).ready(function(){
            $('#textarea').summernote();
        });
        //----Show Sub Category

        //remove image
        function removeImage($this)
        {
            $this.parent().remove();
            var images = '';
            $.each($('.images'), function (i, val) {
                images += $(this).data('src')+',';
            });
            $('#new_image').val(images);

        }


        $('#category').on('change',function (e) {
            var cat_id = e.target.value;
            //Ajax
            $.ajax({
                url : "{{ url('admin/subCat') }}/"+cat_id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function (result) {
                    $('#subcategory').empty();
                    $.each(result.data, function (index, SubcatObj) {
                            $('#subcategory').append('<option value="' + SubcatObj.id + '">' + SubcatObj.name + '</option>');

                    });
                }
            });
        });
    </script>
@endsection
