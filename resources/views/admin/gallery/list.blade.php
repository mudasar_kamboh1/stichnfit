{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
{!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
@parent
@endsection

@section('content')
<div style="background-color: #fff; padding: 15px 15px 0">

    <div class="row">
        <div class="col-md-4">
            <a href="{{ route('admin::gallery.create') }}" type="button" class="btn btn-info btn-lg">Insert Image</a>
        </div>
    </div>
    <br>
    <div class="table-responsive list-records">
        <table class="table table-hover table-bordered">
            <thead>
            <!--<th style="width: 10px;"><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button></th>-->
            <tr>
                <th>#</th>
                <th>Image</th>
                <th>Images</th>
                <th>Sub Category</th>
                <th>Name</th>
                <th>Description</th>
                <th>meta title</th>
                <th>meta description</th>
                <th>meta keywords</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
             <?php
                $x=1
             ?>
            @foreach($lists as $list)

                <tr>
                    <td>{{ $x++ }}</td>
                    <td><img src="{{asset('assets/uploads')}}{{ '/'.$list->image }}" style="width: 70px;height: 70px"></td>

                    @if($list->images)
                        @php
                            $list->images = explode(',',$list->images);
                        @endphp
                        <td>
                            @foreach($list->images as $image)
                                 <img src="{{asset('assets/uploads')}}{{ '/'.$image }}" width="30px" height="30px">
                            @endforeach
                        </td>
                    @endif

                    <td>
                        {{  !is_null($list->sub_category) ? $list->sub_category->name : ''}}
                    </td>
                    <td>{{ $list->name }}</td>

                    <td>{!! \Illuminate\Support\Str::words($list->description, $words = 25, $end = '...') !!}</td>
                    <td>{{ $list->meta_title }}</td>
                    <td>{{ $list->meta_description }}</td>
                    <td>{{ $list->meta_keywords }}</td>
                    <td>
                        <div class="btn-group">
                            <form action="{{ route('admin::gallery.show',$list->id) }}" method="get">
                                <input type="submit" class="btn btn-primary btn-sm" value="Edit" ></input>
                            </form>
                            <form id="formDeleteModel_1" action="{{ route('admin::gallery.destroy',$list->id) }}" method="POST" class="form-inline">
                                @csrf
                                {{ method_field('DELETE') }}
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete"></input>
                            </form>
                        </div>
                        <!-- Delete Record Form -->
                    </td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
</div>



@endsection

@section('footer-extras')


@endsection
