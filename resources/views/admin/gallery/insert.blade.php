{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Orders')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <div style="background-color: #fff; padding: 15px 15px 0">

        <div class="row">
            <div class="col-md-4">
            </div>
        </div>
        <br>
        <div class="table-responsive list-records">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Insert Data</h4>
                </div>
                <div class="model-body">
                    <form action="{{ route('admin::gallery.store')}}" method="POST" id="form-data" enctype="multipart/form-data" >
                        @csrf
                        <div class="modal-body">
                            category :<select name="category" id="category" class="form-control">
                                <option>Select</option>
                                @foreach(App\Category::all() as $options)
                                    <option value="{{ $options->id }}">{{ $options->name }}</option>
                                @endforeach
                            </select>
                            subcategory :<select name="subCategory_id" id="subcategory" class="form-control">
                                <option value=""></option>
                            </select>
                            Title :<input type="text" name="name" class="form-control">
                            Post :<input type="file" name="image" class="form-control">

                            Images :<input required type="file" class="form-control" name="images[]" placeholder="address" multiple>

                            Description :<textarea type="text" id="textarea"  name="description"></textarea>

                            <div class="form-group">
                                <label for="usr">Meta Title:</label>
                                <input type="text" class="form-control" id="meta_title" name="meta_title" />
                            </div>
                            <div class="form-group">
                                <label for="usr">Meta Description:</label>
                                <input type="text" class="form-control" id="meta_description" name="meta_description" />
                            </div>
                            <div class="form-group">
                                <label for="usr">Meta keywords:</label>
                                <input type="text" class="form-control" id="meta_keywords" name="meta_keywords" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" class="btn btn-success" value="Insert">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-extras')


    <script type="application/javascript">
        //Tiny Editor----Start

        $(document).ready(function(){
            $('#textarea').summernote();
        });
        //Tiny Editor----End
        //----Show Sub Category
        $('#category').on('change',function (e) {
            var cat_id = e.target.value;
            //Ajax
            $.ajax({
                url : "{{ url('admin/subCat') }}/"+cat_id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function (result) {
                    $('#subcategory').empty();
                    $.each(result.data, function (index, SubcatObj) {
//                        alert(SubcatObj.categories_id);
                        if(SubcatObj.category_id == cat_id)
                        {
                            $('#subcategory').append('<option value="'+ SubcatObj.id+'">'+SubcatObj.name+'</option>');
                        }
                    });
                }
            });
        });
    </script>
@endsection
