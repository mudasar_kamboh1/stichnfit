<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li class="{{ \App\Utils::checkRoute(['dashboard::index', 'admin::index']) ? 'active': '' }}">
        <a href="{{ route('dashboard::index') }}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
    </li>
    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::users.index', 'admin::users.create']) ? 'active': '' }}">
            <a href="{{ route('admin::users.index') }}">
                <i class="fa fa-user-secret"></i> <span>Users</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::orders.index', 'admin::orders.create']) ? 'active': '' }}">
            <a href="{{ route('admin::orders.index') }}">
                <i class="fa fa-user-secret"></i> <span>Orders</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::gallery.index', 'admin::gallery.create']) ? 'active': '' }}">
            <a href="{{ route('admin::gallery.index') }}">
                <i class="fa fa-user-secret"></i> <span>Gallery</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::category.index']) ? 'active': '' }}">
            <a href="{{ route('admin::category.index') }}">
                <i class="fa fa-user-secret"></i> <span>Category</span>
            </a>
        </li>
    @endif

    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::contacts.index']) ? 'active': '' }}">
            <a href="{{ route('admin::contacts.index') }}">
                <i class="fa fa-user-secret"></i> <span>Requests</span>
            </a>
        </li>
    @endif
    @if (Auth::user()->can('viewList', \App\User::class))
        <li class="{{ \App\Utils::checkRoute(['admin::subCategory.index']) ? 'active': '' }}">
            <a href="{{ route('admin::subCategory.index') }}">
                <i class="fa fa-user-secret"></i> <span>SubCategory</span>
            </a>
        </li>
    @endif
</ul>
