<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="telephone=no" name="format-detection">
    <title></title>
    <!--[if (mso 16)]>
    <style type="text/css">
        a {text-decoration: none !important;}
    </style>
    <![endif]-->
    <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
</head>

<body>
<div class="es-wrapper-color">
    <table class="es-wrapper" style="margin-top:20px;" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td class="esd-block-text es-p10t es-p15b" align="center">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABkCAMAAAD0WI85AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjZDMUIxMjAwRjNENzExRThCNTdGQkIyN0VCNjQ2MzA1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjZDMUIxMjAxRjNENzExRThCNTdGQkIyN0VCNjQ2MzA1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NkMxQjExRkVGM0Q3MTFFOEI1N0ZCQjI3RUI2NDYzMDUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NkMxQjExRkZGM0Q3MTFFOEI1N0ZCQjI3RUI2NDYzMDUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7jrW6oAAADAFBMVEXNGITua6iSkZb4y+KcbK9eJmzpWaP04e3pTp3e0eGPaJlMGE7nutfautCJIWbzo8zr7OznTJypqKvthLvnxtzWibn86/SxbZrZRpi1JoKGHGLb3N3BqMiNR3by7PLXP5NUKGC3N4d7Tofs4u3S0tP////j4+TwhrzGscq0kMJ4N5F3KF1kG1HJMou1WI/6+frDw8X63OvImLjSqcTKy8zybqr+9fr1vNrolsf19fbsc7Knhq67u716RGzFXaHYtMuWOHnwfbM7E0hqJmy1mr7ymsb1sdTMYpjylcXBU5qzs7Xw5u/u0eSkeLXHZaa7g6apirHznsn3wdzBLIf++fy5oL9kLnLv7/C3eKL75PB6I2bsTp7Mt9Ps3+newdTSJIlrOnmKN33YYKT49/j8/PyTXqX88PbazN7Aiq2oRInuebXqY6ng1eTSVZeBImT5z+W7Sorm3OmnO4TLRJHCQIrmcq6kVYrcnMaKUp+yIH7nUp/HrdLVwN30qM+2QpWdRoHFj7X28PXzyuJ1LXSuHHzposyWVYWvsLKCWIz99/vhSZr+/f7r2eSBFF3hRpjty+F4N2WZmZzj2eX4vNvRwdXlSpu2ubqdfKT3cKzgZKKeGHOukbX20+asGnrAXpJXJGrRgLbwjL9sO2D38/eNKmy2trh0HVxUImK/wMHY2dnqr9OMJWmEMWbXyNvMb6yeOYPUw87IycqAQ5hZJGb/+/3hQZb98vi6tbvOor/Xjr7Fx8itra/OztDpZaSEJm34x+AyDkH61OdNHlvraKzsSZ3byNOfoKPYk8D78/igMnTvf7eQLm/paKViKW7dn8j7+vv9/f2tG3vWLY7V1tf+/v72tNbo5+jz8vPf3+CEMHlzQH/TRZL26fG4P43ROZD2+vnxkMLyj73wb7DeOZSsGHrvTqDsb6/a2Nvn0d/k1+nLx8zoTZyVUp/x1uj22Oe6obD1t9j4/PqpYZGKi467wL/PebLdeq/lj8GLLWfWl8DVmsGknKT09PTldLTkerf///9DOBbDAAABAHRSTlP///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////8AU/cHJQAADn5JREFUeNrUmwlcE1cawJUYDCghAxgEVJYbIq6gBgqlgnIUQVQEOYoicomgoNX1QCkeKFehReWwFqm1IOhqV8SDY4toS62KAYnAKlLWq7bpwapb93D79r2ZSTITAgRMbPj0BzPzZua9//uu9948xgCVSfDuF9E9K8cgeWhcYf+HKG3baaqrbYyqXsx9Y/IYUj6yv3Tp0vz5Ey5fDinWGmUgTi9uiTEeTrh0CQdBJMeZxRtGE0iKRBtjHs6/RIJAjsvHz54NWZY7akBOrBwjow9SI8ePn2Uyz2pbjBKQvWOkYg8JCIz5hEKYUKIOjgqQPgrHR5cvT5hPCPIQEoTZMmUUgDjfooAEeHkdLs6KgjSXpQpBcljtQRipFJAc4trLtV5Zx2kcyidRNoijpogC8pu0QKs4hMqhdBIlg8QnYVSQN6hlG4qZNCmOVV+QmnkYxqKAnKAXnzKnkWiTI5ZTccvVDUQXw0SsB1KQ2zLlMYdDqCTmq9FF/61249QMJIWFQY3kS0H29rtFS5tKEnJ4JgBedloB6gXidAVDIpLmdX05d9nS7Mv84NVx/gHb1QskUISDUGwrWt5tx5bdoaHEfT/uqrhM4N/6+4OEp2KEFEpU0pMt985pNJSWqK0HLVwQ4X6vwxvUQCNHSQ6qSnYPcK+LbZbU7XktzJA7WVnmUSFLpqmBaUkUQvUS/YHvX3uYZGnhkZ7/SlMV5YF4SzgogWsA2xKnyCnFWSEhUSHMkCjz4lecBysNJDtJCiJinRsgJfZnidP6ccf+DdPUJ2qlYFSRkEyuGfwxuyk7ApRSv9JA0jA6yYMBcyJVdsX5j9uhXiA2mAzJOcLjb3UP9tTyAIu4GPUCcZUBwURkOoke7KlxdgF26jX6FSbJgmAs0YP+Y3m6+FfzeP5qNoy/gvUXVv6twSPXdh4vAKgZSKJIHgnrHERZOXGAZ3LjeLwd6gbijckVkQihDKCT73m8rTPVDaQRG0BYrPwHK1/ITyI8nh1QN5DspIFIYKLHzv3Psf8jMVt51RvUDgT4YIMIi5Xq6hMu88QOHm8cUD+Q4MfYEJKa6MylPhHA4y1XQxDgqIkNKZq+TdLpVTVv61W1XA5ipA5Ngolc+85LkogdUEsQwFBAJ1CSdLlkEtmgpiDA8bFCJNDCwsEKZbq60pdMnQJTFUQJnKRMV1fBaryjvmIkWM6aH3PVGQSAJptUkSIkEZrO6g0CQHja/QiWSAGaeY4qBrna3d3NfZXhXLyzflIhizUkS2pajepA4m/ONTOC8t6iLWhQMXHLTbps6V6ILm1ZyL0pW0KM17tvzn3PyMwsKMhUMwlf1h5MOYnkwEW2mongA6IWYCDbgL0+8YqAnDDLgFJaWrrNqCcVhslFGbJiMAn/VbuwTbZkARo91pqh620P4SvManf3+V7LySmEA0cWxnoEJSIC/kcihtNcile7QOZVi8A/8d+TwGmimra20tKMbUEzTPMxzFEBkJvoGUgR1JMvgqahyT3dH6QW//X1QtmCtrkAjF/Q1oZXiiQj43QwCDbbZmQUFPQi7UXYkSPrzZBs2nR3co4IYiGFiXRRvXP7gXxCdBc4jTeoNMMoqMc0RxSB7FUBkJ+MCIoc1IVQjtYsUhwkYy4Yf6+0tC0DtXzGjB7Tc/k5j+OzzXCwhfDtLzv2JU+fPv1D+wkT7O2n/+ubTZNFjyIwrDd4UJDStm1GM3ryoc+JnU4BkNOlRkGmORGpmnhiE2ly5ZlWLertNjkgC3KjZ/T0mObnF8J+QCJiaYYHQ5DShw8NiJXuv9449PaRP9vbT4AwEGf6NxsLv8T0gwmQNiOJnIYgSBG14FdkTSyiXzFFQbLXmRZC8+115IanpLliGIz0CyciWVCKevUT/Di+Fu/uwKv4WS3e3/fw45/6WKkRqP2ipMeaeO8lckG8GTSLGRs/JmbE/LF8fsm7xhVvIbFHQFOPiL60ySU0csJ/PCnd4JNSpIi9wLGRwWhkEAuAV+AR/Oc0JMj5JFx7RCTJdfaVFCzCzYVcRQjEe1uXODFo27bNKIhYvcolp4n6TdkgnpGIYd7QaM5Hrzt0g88netF7LJSSNcbG7lUXMjOzIMmnGs+K347wXYRHiA8obXlhCr2UlSZOs/iLXRUNv+eJlrjSZ0BQfkPmErGUBMFvIkF2Q0fARInUiTvJX9OLfwzNvsbnw9ZTQPgQJFLY8YND5nR7+w8LNDQ0pt7tKUUaWUipMhDvU13a2rLCIDX3YYBEL9BMDEyhKtAGTwbyQBiwRCRypSylXAmmDyTnocaPtfk7ElcJiJXVy+/a36yo+LD+vwUFn1ZsCkKON+mEMy4p4peJRggCdn5+b+NkLALhYEne0lk2MRSUC0Kp4Rp+nAbkgVCE/zkOYiXoMNlnP/VC5i96b9lXmOGhKYfQ6TVJr4wUxD+qjOm5ZxWO8+iRZp+CIKRGiOkIYwgQfvQ7bBzEKrbzL3lTGyovXIBun7wJ5ghxfLV5ZY2A/ebMMmZZGY7z7r2NfcPSyBWFQErSK0gQq9Z/hE3tsvzBYWpFRcV1TDqSsXlljQDgskz8IRzh3CG/JdkgW5MLkgKHHGIfScQLfOSCJM5Dch8e3dhDaiTXykq4wpBjbVK1D4Ikb5SmimuvrhEosVpe2lHiT662xLUwaGvyo5bBu5vuFrKIqEV82H0slAfycy4SGLVKFpSRIJFP4I8VnaEdnKeQI9mMJfK+7YMLQwkaIddqThWHUHcjaeO2pkOC4EM+soqDZWUte/5mSFmkEwWSsVx/KQWECL+B0EVWiUFmbxbCnzNjOr81zEtOhk5CS9mvqJG1xavJo2IaCG5qpHr+vXFyYcQjMQhekkXMqMgZuzdMQ8KUx0RCpIAInvzK50e3iEHc82YjAxPMFCIQ9t2IiKZ+y+IjBsliMrWXaU2LnalFuIqXFERiZ28yoQ4+/48UhMkkQKBt4ZFbpJloQ0QwV0eQjYPwf+7WWZxuvI5fsqaMSYDkuud5dCOPj0Ug7GSPe3c/VppGbAnPiLpzp4U4OiUHZAkeCJbIAQlfhUIditziCQerqeYaDvLH62wo6/g39ohBoEbywpBxCa3C8tjsd6CNrlaWRvyj6LvcmOYC+SBSo6OBAFsicnvACJCDz6FcswmNjP3GGHIYr4OuziRBhO7GRGK0EiIQT2m3kZ4oGrlGlslwhIi7SFEQMKWFCNwoAkCcQ+4J6TdK+GIQ9rqSVRIQqBG2cfoxRAJBkpkyIF9vzGE9ikgbGUjMFNpuqjuSncYyPoKEZlrmklecoqShMk8PNvvk+uhDY/klJMihFgLEADZfB+ohL4EEYcqCFDOhI24Sf6cnBhBXFHd2wcFicyLwhmR5SXdWHNZGQurHFj8hqdbiJ0uonYGyEFSMp2cyajvbEw0S1uMuwl6/xtMTXTfeqaOjkwDZjE/OhkdPjfG3aO+ntGQKtRbgqI8kcFh5RGBxavny5acsRr5Sc8zCdt8XBAUh0BnEB+IreXl5xHEeOrqolgt0wH9nOns4ct1d8XebdL02EP/F14eFwd78ZBhvr7R+XSAXPYaHcX2nYrtRuvBM0Fru8npAxj8dJkbYCsVeHFvfiYcSP+FrAYkcnjo8EgbBcKE12SWztcMBgFA3EAs12OnQ0GGiQhD3z4ajjKfuxwbq/lgArJtpm+dDM0GRhgvocoh1a7Y2KdArqOSoDiTy5DA8fNaKQT1aUFBPu8Rxi8ksDwWWltYFmeVudXp+KjQtf4XtKj3hyWCfq2L1OkGlRpX4zHIONKauuqozlR3AwaTzjEAILPXmqBDEUEHHWGwgGPxFHX7gTKUbZ2YHEHAyQeezBisA2hvcOHMagF8ousHazdJSdSBPFHIMQx2aYwjkxd6GOe1+MZnCyubvmvWaO+v10DVLPz/QkdlVjo6FRaGy6USZIGFDUpx8OluyYS6Ug4fT9jPyUkZ9+becclBQUPfcsr3IBM8cHQXQ1Nr9OvAY7MbJbFUZiGAoD/ki/X1JSx38itzckJU4tOMXOqGldELlODRzOLDdL2Gb60CddDQiDM7tpPhQkZ+16vLI+ycHT3xxPPEf7VX5uZm8RFka9mo5onEJDeWAhmeVoF0DhlUidTd8xeWGpzD6fHR7fY/2Buof1fX1uX37QGN4fLwTiIlRYUKMHCxKzbKo2VpNmBXHr3IO2dVzBK1+gjrr2ILyM12hGg11dc2Zes8BCI7nNh1ImDTPptenb2kjI4XBaAoPP+/k6OjYBI8bmxgp3HCnGpWB6AxE8ZlhZDbanUXsw2zQ6wJ1ZMjhwOxgrVFfqeFiUl/U0BrbMbOuOfK27xVX37TGX74aeF+9kxPUVjCoea0a8UggPCOAh0/LquqhVbh1AYFb/cuqopfNlV3P6jqhG4fCvJLL1TFc/Ku3M4MLByfWVUPUlytUEcgTeT6SfnEXmSx5cXgKjEHRJhN6RLNeuZ+fm4ODoIP8WHag9/6fFjf+MsL9/koE2dU/anlclOw6mMKT/iGVIDMGmDwXvBQKWl2K8JEhd6mvzdHbjj+YtBYJf/8ZYr8BfJi0d2PjqndJh02UcVIs2iZhk3h0Kfqs9JzjVgV+f5CLMurQoZRpUXfLddRRShzTen0byU117fUN6jBn96fNcNNpg1s7HvVUYj5OB3y9nSlf6ka+/0Wpg0bqGCWdtnk/plrebrlw3URdZe0PUiqIhVQl19+nlezgrZaD8ViXq7S6lTuxmiUBmUUvCNja2g/jmvN5JVatXJDYzWJH30X/bMST+SPWcG+bpcFKrVrJc/YVZC4xpF/eXk2fnAsZjTXKrVj5qyhEet9JvxpnB1QtSl/Xcj/Z30Usqi1GHwhwx3M67ZJXABiFIMAdBuF06uqCME5rVIIAA+jxkZTzteOsRicIsNjM3kxZtvJaDkYpCKhJYCdIB+12x0YtCAzD6bPFh99vB6MYBOzaGUlal8W01wHyfwEGAEbSZ6zEXFzEAAAAAElFTkSuQmCC" alt="stitchnfit">
                </td>
            </tr>
        </tbody>
    </table>
    <table class="es-content"  style="margin-top:20px;background: #f7f7f7;width: 600px; padding: 10px 30px;border: 1px solid #ccc; border-radius: 10px;" align="center">
        <tbody>
        <tr> <td><h3 style=" padding: 4px 0;">
                Order Details
            </h3></td>
        </tr>
            <tr>
                <td><span style="font-size: 14px; line-height: 150%;"><b>Token:</b></span></td>
                <td><span style="font-size: 14px; line-height: 150%;">{{ config('constants.order_token_prefix').$order->token }}</span></td>
            </tr>
            <tr>
                <td><span style="font-size: 14px; line-height: 150%;"><b>Received at:</b></span></td>
                <td><span style="font-size: 14px; line-height: 150%;">{{ \Carbon\Carbon::parse($order->created_at)->format('d-m-y h:s:i: a') }}</span></td>
            </tr>
            <tr>
                <td><span style="font-size: 14px; line-height: 150%;"><b>Name:</b></span></td>
                <td><span style="font-size: 14px; line-height: 150%;">{{ !is_null($order->user)? $order->user->name : 'N/A' }}</span></td>
            </tr>
            <tr>
                <td><span style="font-size: 14px; line-height: 150%;"><b>Mobile:</b></span></td>
                <td><span style="font-size: 14px; line-height: 150%;"><a style="text-decoration: none" href="tel:{{ !is_null($order->user)? $order->user->mobile : '' }}">{{ !is_null($order->user)? $order->user->mobile : 'N/A' }}</a></span></td>
            </tr>
            <tr>
                <td><span style="font-size: 14px; line-height: 150%;"><b>Email:</b></span></td>
                <td><a style="font-size: 14px; line-height: 150%;"><a style="text-decoration: none" href="mailto:{{ !is_null($order->user)? $order->user->email : '-' }}">{{ !is_null($order->user)? $order->user->email : 'N/A' }}</a></span></td>
            </tr>
        </tbody>
    </table>
</div>
</body>

</html>
