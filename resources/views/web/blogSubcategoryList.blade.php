<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--Shirt gallery banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/gallery.png')}}" alt="">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">Blogs</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="single-gallery-section">
    <div class="container">
      @if(count($posts))
          @foreach($posts as $post)
            <div class="blog-description">
                <div class="row">
                    <div class="col-md-3">
                    <img class="mr-3" src="{{asset('/assets/uploads/'.$post->image)}}" class="img-responsive" alt="" width="100%">
                    </div>
                    <div class="col-md-9">
                        <h5 class="mt-0 text-left"> {{ $post->name }}</h5>
                        <div class="news-tags text-left"><span class="date">{{\Carbon\Carbon::parse($post->created_at)->format('d-m-Y h:i A') }}</span></div>
                        <p class="text-justify"> {!! \Illuminate\Support\Str::words($post->description, $words = 22, $end = '...') !!}</p>
                        <a href="{{ url('blog/'. $post->sub_category->slug.'/'.$post->slug) }}" class="btn view-btn">Read More</a>
                        <div class="social-media-icons ss">
                            <ul>
                                <li class="fb"><a href="https://www.facebook.com/stitchnfitworld" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="twit"><a href="https://www.instagram.com/stitchnfitworld" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li class="inst"><a href="https://www.twitter.com/stitchnfitworld" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
          @endforeach
      @endif

    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
