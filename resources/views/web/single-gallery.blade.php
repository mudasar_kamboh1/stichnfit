<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
@if(!is_null($post))
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/gallery.png')}}" alt="" width="100%">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">{{$post->name}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=single-gellery-section">
<div class="container gallery-container">
    <div class="gallery-description">
        <a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>
        <h2>{{$post->name}}</h2>
        <p>{!! $post->description !!}</p>
    </div>
    <div class="tz-gallery">
        <div class="row">
                @foreach(explode(',', $post->images) as $image)
                    <div class="col-md-3">
                        <a class="lightbox" href="{{asset('/assets/uploads/'.$image)}}">
                        <img src="{{asset('/assets/uploads/'.$image)}}" alt="" width="100%">
                        </a>
                    </div>
                @endforeach
        </div>
    </div>
</div>
</section>
@else
<div class="container" style="padding: 50px 0;">
    <div class="gallery-description">
<h2 class="text-danger danger">Not Found!</h2>
<a href="{{url('/gallery')}}" class="btn view-gallery">View Gallery</a>
    </div>
</div>
@endif
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
