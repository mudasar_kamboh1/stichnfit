<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
<section class="notfound-section">
    <div class="container">
<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>Oops!</h1>
        </div>
        <h2>404 - Page not found</h2>
        <p>The page you are looking for might have been removed had its name changed or is temporarily unavailable.</p>
        <a href="{{url('/')}}">Go To Homepage</a>
    </div>
</div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
{{--// Including assets Section--}}
@include('web.partials.assets')
</body>
</html>
