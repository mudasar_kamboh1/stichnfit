<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit - Contact us | Online Stitching & Tailoring Services</title>
    <meta charset="UTF-8">
    <meta name="description" content="Contact us for online tailoring & sewing services of ladies kurtas, shalwar suits, trousers, blouse, kurtis, gowns and all type designer dresses and get it stitched within 4 working days.">
    <meta name="keywords" content="online tailoring, sewing service, online stitching, stitching, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--contact us banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/contact-us.png')}}" alt="ContactUs">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">Contact Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--Contact us description section--}}
<section class="contact-section">
    {{--<div class="container">--}}
        {{--<div class="col-md-12">--}}
            {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
        {{--</div>--}}
    {{--</div>--}}
<div class="container">
    <div class="contact-description">
        <p>Don’t Put an Arzi – Get in touch and Book a Darzi</p>
        <p>Fill out the contact form below or send us an email at info@stitchnfit.com and we will get back to you!</p>
        <ul>
            <li><i class="fas fa-phone"></i>&nbsp;Phone : <a href="tel:04232532277">042-32532277</a></li>
            <li><i class="fab fa-whatsapp"></i>&nbsp;Mobile : <a href="tel:03093333763">0309-3333763</a></li>
        </ul>
    </div>
    <div class="contact-form-section">
        <form action="#" method="POST" class="contact-form" id="contact-form">
            @csrf
            <div class="row">
                <div class="col-md-4">
                        <div class="form-group left-inner-addon">
                            <i class="fas fa-user"></i>
                            <input type="text" class="form-control cstm-input" name="name" maxlength="50" placeholder="YOUR NAME">
                        </div>
                </div>
                <div class="col-md-4">
                        <div class="form-group left-inner-addon">
                            <i class="fas fa-phone"></i>
                            <input type="text" id="phone"  class="form-control cstm-input" placeholder="YOUR PHONE NUMBER" name="mobile" onchange="$('#phone-number').val(getId($('#phone').val()))"  minlength="11" maxlength="11">
                            <input type="hidden"  id="phone-number" name="mobile">
                        </div>
                </div>
                    <div class="col-md-4">
                        <div class="form-group left-inner-addon">
                            <i class="fas fa-envelope"></i>
                            <input type="email" class="form-control cstm-input" name="email" placeholder="YOUR EMAIL" maxlength="50">
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group left-inner-addon-textarea">
                        <i class="fas fa-comment"></i>
                        <textarea placeholder="YOUR MESSAGE" name="message" maxlength="1500" class="form-control cstm-textarea"></textarea>
                    </div>
                </div>
            </div>
            <div class="text-left">
                <button type="submit" class="btn btn-default contact-btn" onclick="insert()"><span id="loader"></span>SUBMIT</button>
            </div>
        </form>
    </div>
</div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
{{--// Including assets Section--}}
@include('web.partials.assets')
</body>
</html>
