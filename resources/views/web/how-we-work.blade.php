<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit - How we work | Online Tailoring & Stitching Services</title>
    <meta charset="UTF-8">
    <meta name="description" content="We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps within a record time of 4 working days. It just doesn’t get any quicker than this!. Stitch n Fit offers sewing and alteration services tailor-made to your requirements and sewed to perfection.">
    <meta name="keywords" content="online tailoring, sewing service, online stitching, stitching, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--who we are banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/how-we-work-banner.png')}}" alt="how-we-work">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">How We Work</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--who we are description section--}}
<section class="how-we-work-section">
    <div class="container">
        {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
    <div class="row no-gutters">
        <div class="col-md-12">
            <p>Stitch n Fit works on 5 simple steps:</p><br>
            <p class="bold"> <span>1.</span> &nbsp; &nbsp;Schedule Your Pickup</p>
            <p class="text-justify">Place your orders online through our website by filling out the form or call us to schedule your pickup. Our tailor master will visit you at your prescribed time for the collection of clothes for stitching or alteration as the case may be. You can describe your desired design and style to our tailor master so that the chances of error are eradicated.</p>
            <p class="bold"> <span>2.</span> &nbsp; &nbsp;Handover Your Dress Material</p>
            <p class="text-justify">Provide your unstitched fabric and a garment for reference to our representative. Please ensure that the measurement garment is the right size so that we may stitch your dream dress properly. </p>
            <p class="bold"> <span>3.</span> &nbsp; &nbsp;Stitched to Perfection</p>
            <p class="text-justify">Stitch n Fit team will stitch the dress as per your requirements with complete perfection. We know the importance of correct fitting and that’s why we put more emphasis on it.</p>
            <p class="bold"> <span>4.</span> &nbsp; &nbsp;Quality Check</p>
            <p class="text-justify">Our quality assurance department makes sure that the stitched dress is a perfect depiction of what you had in mind. They make sure that the finishing is perfect, the dress is of the right size, and there are no flaws whatsoever. </p>
            <p class="bold"> <span>5.</span> &nbsp; &nbsp;Delivered to Your Doorstep</p>
            <p class="text-justify">After the quality check is completed, we deliver your dress within a record time of 4 working days. It just doesn’t get any quicker than this!</p>
        </div>
    </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
