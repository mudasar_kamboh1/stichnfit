@extends('web.other-layout')
@section('content')
    <section class="inner-banner-section" style="background:#ebebeb;">
        <div class="carousel">
            <div class="carousel-inner">
                <div class="carousel-item2 active">
                    <img class="d-block w-100" src="{{asset('assets/img/about-banner.png')}}" alt="">
                    <div class="carousel-caption cstm-caption">
                        <div class="col-md-12">
                            <h1 class="text-center">My Orders</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Order track info section start here -->
    <div class="order-track-detail-section">
        <div class="container">
            <div class="col-md-12">
                <a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>
            </div>
        </div>
        <div class="container">
            <div class="order-track-info">
                               <p class="desktop-hide"><span class="track-order-id"><strong>Order Token:</strong> {{ !is_null($order)? config('constants.order_token_prefix').$order->token : '' }}</span><br><br>
                    <span class="track-order-date"><strong>Order Placed:</strong> {{ !is_null($order)? \Carbon\Carbon::parse($order->created_at)->format('d, M, Y h:i:s A') : '' }}</span></p>
                    <span class="track-order-date"><strong>Status:</strong> <strong>{{ config('constants.order_status.'.$order->status) }}</strong></span></p>
                <div class="order-track-detail">
                    <div class="col-md-12">
                        <div class="wizard">
                            <div class="wizard-inner">
                                <ul class="nav nav-tabs cstm-tabs" role="tablist">
                                    @foreach(config('constants.order_status') as $k =>  $status)
                                                    @if($k <= 6 && $order->status <= 6)
                                                        <li role="presentation" class="{{ isset($order->status) && $k == $order->status? 'active':''}}">
                                                            <a  data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                                            <span class="round-tab">
                                                               <i class="fas fa-check"></i>
                                                            </span>
                                                            </a>
                                                            <p>{{$status}}</p>
                                                        </li>
                                            <div class="connecting-line"></div>
                                                    {{--@elseif($k > 6 && $order->status > 6)--}}
                                                        {{--<li role="presentation" class="{{ isset($order->status) && $k == $order->status? 'active':''}}" style="width: 30%;">--}}
                                                            {{--<a  data-toggle="tab" aria-controls="step1" role="tab" title="Step 1" style="margin-left: 20%;">--}}
                                                            {{--<span class="round-tab">--}}
                                                                    {{--<i class="fas fa-times"></i>--}}
                                                            {{--</span>--}}
                                                            {{--</a>--}}
                                                            {{--<p>{{$status}}</p>--}}
                                                        {{--</li>--}}
                                                    @endif
                                    @endforeach
                                </ul>
                                {{--<ul class="nav nav-tabs cstm-tabs" role="tablist">--}}
                                {{--@foreach(config('constants.order_status') as $k =>  $status)--}}
                                    {{--@if($k <= 6 && $order->status <= 6)--}}
                                        {{--<li role="presentation" class="{{ isset($order->status) && $k == $order->status? 'active':''}}">--}}
                                            {{--<a  data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">--}}

                                            {{--</a>--}}
                                            {{--<p><span>Your Order is </span>{{$status}}</p>--}}
                                        {{--</li>--}}
                                    {{--@elseif($k > 6 && $order->status > 6)--}}
                                        {{--<li role="presentation" class="{{ isset($order->status) && $k == $order->status? 'active':''}}" style="width: 30%;">--}}
                                            {{--<a  data-toggle="tab" aria-controls="step1" role="tab" title="Step 1" style="margin-left: 20%;">--}}

                                            {{--</a>--}}
                                            {{--<p>{{$status}}</p>--}}
                                        {{--</li>--}}
                                    {{--@endif--}}
                                {{--@endforeach--}}
                                {{--</ul>--}}
                            </div>
                        </div>
</div>
                    <div class="col-lg-12 col-sm-12">
                        <div class="table-responsive mobile-hide">
                            <table class="table table-bordered">
                                <tr>
                                    {{--<th>Name</th>--}}
                                    <th>Image</th>
                                    {{--<th>Email</th>--}}
                                    <th>Mobile</th>
                                    <th>Pickup Schedule</th>
                                    <th>Location</th>
                                    <th>Note</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
{{--                                    <td>{{ !is_null($order->user)? $order->user->name : '' }}</td>--}}
                                    <td>
                                        @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                                            <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="100" height="100">
                                        @else
                                            <img src="{{asset('assets/img/download.jpg')}}" alt="Order Image" title="" width="100" height="100">
                                        @endif
                                    </td>
{{--                                    <td>{{ !is_null($order->user)? $order->user->email : '' }}</td>--}}
                                    <td>{{ !is_null($order) && $order->additional_phone ? $order->additional_phone : (!is_null($order->user) ? $order->user->mobile : '-- -- --') }}</td>
                                    <td>{{ !is_null($order) && $order->pickup_time? \Carbon\Carbon::parse($order->pickup_time)->format('d, M, Y h:i A') : '-- -- --' }}</td>
                                    <td>{{ !is_null($order) && !is_null($order->area) ? $order->area->name: $order->user->address }}</td>
                                    <td>{{ !is_null($order) && !is_null($order->notes) ? $order->notes : '-- -- --'}}</td>
                                    <td width="200" align="center">
                                        <a href="#" class="track-order set-pickup-schedule" data-token="{{ $order->token }}">{{ is_null($order->pickup_time) ? 'Set Pickup Schedule' : 'Edit Pickup Schedule' }}</a>
                                        <a class="track-order" href="#" data-toggle="modal" data-target="#add-mobile">{{ is_null($order->additional_phone) ? 'Add Additional Mobile Number' : 'Edit Additional Mobile Number' }}</a>
                                        <a class="track-order" href="#" data-toggle="modal" data-target="#notes"> {{ is_null($order->notes) ? 'Add Note' : 'Edit Note' }}</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="mobile-card web-hide">
                            <div class="card mb-4">
                                @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                                    <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="100%" height="170">
                                @else
                                    <img src="{{asset('assets/img/download.jpg')}}" alt="Order Image" title="" width="100%" height="170">
                                @endif
                                                {{--<img class="card-img-top" src="https://placeimg.com/290/180/any" alt="Card image cap">--}}
                                                <div class="card-body">
                                                    <span><strong>Mobile:</strong></span> <span> {{ !is_null($order)? $order->additional_phone : (!is_null($order->user) ? $order->user->mobile : '-- -- --') }}</span><br><br>
                                                    <span><strong>Pickup Schedule:</strong></span> <span>{{ !is_null($order) && $order->pickup_time? \Carbon\Carbon::parse($order->pickup_time)->format('d, M, Y h:i A') : '-- -- --' }}</span><br><br>
                                                    <span><strong>Location:</strong></span> <span>{{ !is_null($order) && !is_null($order->area) ? $order->area->name: $order->user->address }}</span><br><br>
                                                    <span><strong>Note:</strong></span> <span> {{ !is_null($order) && !is_null($order->notes) ? $order->notes : '-- -- --'}}</span><br><br>
                                                    <a href="#" class="track-order set-pickup-schedule" data-token="{{ $order->token }}">{{ is_null($order->pickup_time) ? 'Set Pickup Schedule' : 'Edit Pickup Schedule' }}</a>
                                                    <a class="track-order" href="#" data-toggle="modal" data-target="#add-mobile">{{ is_null($order->additional_phone) ? 'Add Additional Mobile Number' : 'Edit Additional Mobile Number' }}</a>
                                                    <a class="track-order" href="#" data-toggle="modal" data-target="#notes"> {{ is_null($order->notes) ? 'Add Note' : 'Edit Note' }}</a>
                                                </div>
                        </div>
                        </div>
                        <div id="show" class="text-center">
                          <a href="#order-detail" class="view-detail">view detail</a>
                        </div>
                        <div id="order-detail" class="submit-order-detail" style="display:none">
                           <div class="measurement-section">
                                <h2>Shirt Measurement</h2>
                                <div class="row padd-20">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Shoulder:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->shoulder : 'N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Chest:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->chest : 'N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Waist:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->shirt_waist : 'N/A' }}</span>
                                        </div>
                                    </div>
                                </div>
                               <div class="row padd-20">
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Shirt length:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->shirt_length : 'N/A' }}</span>
                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Arm Length:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->arm_length : 'N/A' }}</span>
                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Arm Width:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->arm_width : 'N/A' }}</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="row padd-20">
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Kalai Style:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->wrist_style : 'N/A' }}</span>
                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Kalai Width:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->wrist_width : 'N/A' }}</span>
                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Hip:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->hip : 'N/A' }}</span>
                                       </div>
                                   </div>
                               </div>
                               <div class="row padd-20">
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Ghera:</label>&nbsp;<span>{{ !is_null($order->measurement)? $order->measurement->ghera : 'N/A' }}</span>
                                       </div>
                                   </div>
                               </div>
                            </div>
                           <div class="measurement-section">
                                <h2>Trouser Measurement</h2>
                                <div class="row padd-20">
                                   <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Trouser length:</label><span>{{ !is_null($order->measurement)? $order->measurement->trouser_length : 'N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Trouser Waist:</label><span>{{ !is_null($order->measurement)? $order->measurement->trouser_waist : 'N/A' }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="contact">Thai:</label><span>{{ !is_null($order->measurement)? $order->measurement->thai : 'N/A' }}</span>
                                        </div>
                                    </div>
                                </div>
                               <div class="row padd-20">
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Gutna:</label><span>{{ !is_null($order->measurement)? $order->measurement->knee : 'N/A' }}</span>
                                       </div>
                                   </div>
                                   <div class="col-md-4">
                                       <div class="form-group">
                                           <label for="contact">Style:</label><span>{{ !is_null($order->measurement)? $order->measurement->trouser_style : 'N/A' }}</span>
                                       </div>
                                   </div>
                               </div>
                                <div class="row">
                                    @if(!is_null($order->measurement) && $order->measurement->belt == 1)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="belt" class="cstm-label">Belt:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($order->measurement) && $order->measurement->half_belt == 1)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="half_belt" class="cstm-label">Half Belt:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!is_null($order->measurement) && $order->measurement->half_elastic === 1)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                          <label for="half_elastic" class="cstm-label">Half Elastic:<i class="fa fa-check" aria-hidden="true"></i></label>
                                          </div>
                                    </div>
                                    @endif
                                        @if(!is_null($order->measurement) && $order->measurement->side_zip === 1)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="side_zip" class="cstm-label">Side Zip:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                    @endif
                                        @if(!is_null($order->measurement) && $order->measurement->front_zip === 1)
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="front_zip" class="cstm-label">Front Zip:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                        @endif
                                        @if(!is_null($order->measurement) && $order->measurement->alround_elastic === 1)
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="alround_elastic" class="cstm-label">Alround Elastic:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                        @endif
                                        @if(!is_null($order->measurement) && $order->measurement->front_plane_round_elastic === 1)
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="front_plane_round_elastic" class="cstm-label">Front Plane Round Elastic:<i class="fa fa-check" aria-hidden="true"></i></label>
                                        </div>
                                    </div>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Additional mobile Number Modal -->
    <div class="modal fade" id="add-mobile" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content cstm-modal-content">
                <div class="modal-body">
                    <div class="order-track-section">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h1>Add Additional Number</h1>
                        <form action="{!! route('webroders.update', $order->id) !!}" method="post" class="order-form" id="mobile-form">
                                @csrf
                                @method('PUT')
                            <div class="form-group">
                                <label for="name" class="form-label">Add Your Additional Number:*</label>
                                <input type="number" id="textbox"  class="form-control cstm-control num" placeholder="Mobile" name="additional_phone" value="{{ !is_null($order) ? $order->additional_phone : ''}}" minlength="0" min=0 maxlength="11">
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                <button type="button"  class="btn btn-default btn-cancel" data-dismiss="modal" >Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- add notes Modal -->
    <div class="modal fade" id="notes" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content cstm-modal-content">
                <div class="modal-body">
                    <div class="order-track-section">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h1>Add Notes</h1>

                        <form action="{!! route('webroders.update', $order->id) !!}" method="post" class="order-form" id="notes-form">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name" class="form-label">Add Your Note Here:*</label>
                                <textarea placeholder="Your Note" name="notes" maxlength="1500" class="form-control cstm-textarea" required="required">{{ !is_null($order) ? $order->notes : ''}}</textarea>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                <button type="button"  class="btn btn-default btn-cancel" data-dismiss="modal" >Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

