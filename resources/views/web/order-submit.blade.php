@extends('web.other-layout')
@section('content')
<!-- Order track info section start here -->
<section class="order-track-detail-section">
    <div class="container">
        <div class="order-track-info">
            <div class="order-message">
                <h2>Thank you for your order</h2>
                <p>Your order's booking has been placed, Our agent will contact you soon on your provided phone number for Details!</p>
            </div>
            <div class="col-md-12">
                <div class="order-detail">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="order-info">
                                <p>Token: <span>{{ !is_null($order)? config('constants.order_token_prefix').$order->token : '' }}</span></p>
                                <p>Name: <span>{{(!is_null($order->user) && !empty($order->user->name))? $order->user->name: 'N/A' }}</span></p>
                                <p>Email: <span>{{(!is_null($order->user) && !empty($order->user->email))? $order->user->email: 'N/A' }}</span></p>
                                <p>Phone: <span>{{(!is_null($order->user) && !empty($order->user->mobile))? $order->user->mobile: 'N/A' }}</span></p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="wizard">
                                <div class="wizard-inner">
                                    <div class="connecting-line"></div>
                                    <ul class="nav nav-tabs cstm-tabs" role="tablist">
                                    @foreach(config('constants.order_status') as $k =>  $status)
                                        @if($k <= 6)
                                            <li role="presentation" class="{{$k == $order->status? 'active':''}}">
                                                <a  data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                                    <span class="round-tab">
                                                        <i class="fas fa-check"></i>
                                                    </span>
                                                </a>
                                                <p>{{$status}}</p>
                                            </li>
                                        @endif
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
    <script type="text/javascript">
        function show1(){
            document.getElementById('measurements').style.display ='none';
        }
        function show2(){
            document.getElementById('measurements').style.display = 'block';
        }
    </script>
@endpush
