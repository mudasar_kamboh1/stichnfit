<section class="news-section">
    <div class="container">
        <div class="new-section-heading">
            <h2 class="text-center">Trends & Fashion News</h2>
            <p class="text-center">Get a complete insight of what’s cooking in the fashion industry </p>
        </div>
        <div class="row">

            @if(!is_null($blogs))
                @foreach($blogs as $blog)
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="news-thumbnail">
                                    <a href="{{ url('blog/'.$blog->slug) }}"> <img src="{{asset('/assets/uploads/'.$blog->image)}}" class="img-responsive" width="100%" alt=""></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="news-content">
                                    <div class="news-heading">
                                        <h2><a href="{{ url('blog/'.$blog->slug) }}">{{ $blog->name }}</a></h2>
                                        <div class="news-tags text-left"><span class="date">{{\Carbon\Carbon::parse($blog->created_at)->format('d-m-Y') }}</span></div>
                                    </div>

                                    <div class="news-description">
                                        <p class="text-justify"> {!! \Illuminate\Support\Str::words($blog->description, $words = 22, $end = '...') !!}</p>
                                        <a href="{{ url('blog/'.$blog->slug) }}" class="btn view-btn">Read More</a>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</section>

