<head>
    @yield('metas')

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="robots" content="noindex, nofollow, noarchive">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="google-site-verification" content="IZgNv0XLV1PYV5v2QVMjqR2SoRSsL1IAFwpAjjCLWdU" />
    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/owl.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/toastr.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/baguetteBox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/bootstrap-datetimepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/fluid-gallery.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/chosen.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/prism.css') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/assets/img/favicon-16x16.png') }}">
    {{--<link rel="stylesheet" href="{{ asset('/assets/css/style2.css') }}">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />--}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-130515960-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-130515960-1');
    </script>
</head>
