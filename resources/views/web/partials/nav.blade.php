<nav class="navbar navbar-expand-lg navbar-light" id="navbar-main">
    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('/assets/img/logo.png')}}" width="100%" class="img-responsive"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <img src="{{asset('/assets/img/OTO.png')}}" width="200px" class="img-responsive img-mobile-hide" style=" margin-left: -30px;">
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav custom-navbar ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/about-us')}}">About Us</a></li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/how-we-work')}}">How we Work</a>
            </li>
            <li class="nav-item">
                @if(\Illuminate\Support\Facades\Auth::check())
                    <a class="nav-link" href="{{ url('my-orders') }}">My Orders</a>
                @else
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#my-orders">My Orders</a>
                @endif
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('blog') }}">Blog</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('gallery') }}">Gallery</a>
            </li>
            @if(\Illuminate\Support\Facades\Auth::check())
                <li class="nav-item">
                    <a class="nav-link inquire-btn" href="{{ url('logout') }}">Logout</a>
                </li>
            @endif
        </ul>
    </div>
</nav>
<!-- My order Modal -->
<div class="modal fade" id="my-orders" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class=" modal-dialog modal-sm">
        <div class="modal-content cstm-modal-content">
                 <div class="modal-body">
                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1>My Orders</h1>
                    <form action="#" class="order-form" id="send-order-otp">
                        @csrf
                        <div id="contact-block">
                            <div class="form-group">
                                <label for="name" class="form-label">Mobile:</label>
                                <input type="text" class="form-control cstm-control" id="track-mobile" onchange="$('#mobile-number2').val(getId($('#track-mobile').val()))"name="mobile" maxlength="11" minlength="11" placeholder="Enter Your Mobile Number" required="required">
                                <input type="hidden"  id="mobile-number2" name="mobile">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-btn" id="orderOtp">Send OTP</button>
                                {{--<button type="button"  class="btn btn-default btn-cancel" data-dismiss="modal" >Cancel</button>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>