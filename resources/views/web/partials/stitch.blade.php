<section class="stitch-section">
    <div class="container">
        <h2 class="text-center">What We Stitch</h2>
        <div class="no-gutters clearfix" style="width:100%;">
            @if(count($designs))
                @foreach($designs as $k => $design)
                    <div class="clearfix float-left" style="width:100%;">
                    <div class="{{ ($k % 2 == 0) ? 'tags-img float-left' : 'tags-img float-right' }}">
                        <div class="article-avatar">
                            <img src="{{asset('/assets/img/'.$design->slug.'.jpg')}}" width="100%" alt="">
                            @if($k % 2 == 0)
                                <span class="left-arrow">
                                    <img src="{{asset('/assets/img/left-arrow.png')}}" alt="">
                                </span>
                            @else
                                <span class="right-arrow">
                                    <img src="{{asset('/assets/img/right-arrow.png')}}" alt="">
                                </span>
                            @endif
                            <div class="article-heading">
                                <h2 class="text-center">{{ $design->name }}</h2>
                            </div>
                        </div>
                    </div>
                    <div class="{{ ($k % 2 == 0) ? 'tags-img float-right' : 'tags-img float-left' }}">
                        <div class="article-categories">
                            @foreach($design->subcategories as $subDesign)
                                <div class="article-category-title">
                                <h3 class="text-center">{{ $subDesign->name }} </h3>
                                <div class="tags">
                                    <ul>
                                        @foreach($subDesign->gallery  as $k => $list)
                                            @if($k == 2)@break @endif
                                            <li><a href="{{ url('gallery/'. $subDesign->slug.'/'.$list->slug) }}">{{ $list->name }}</a><span>|</span></li>
                                        @endforeach
                                            <li><a href="{{url('gallery/'. $subDesign->slug)}}">More</a></li>
                                    </ul>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        </div>
                        </div>
                @endforeach
            @endif

</div>
        </div>
    </div>
</section>