<section class="service-section" style="background-image: url({{asset('/assets/img/inner-bg.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>World-class tailoring services <br>
                    with years of experience</h1>
                <p class="text-justify">Stitch n Fit has a team of highly experienced tailors who are experts in stitching dresses
                    that pleases you in every way possible. We have a long history of using the best materials
                    including strong strings, exquisite buttons, quality laces, and others. Our quality assurance
                    department makes sure that the stitched dress matches the quality standards and from
                    stitching to finishing, there are no flaws in the dress. All of this happens in a record 4-day
                    delivery time which makes us stand out! </p>
            </div>
            <div class="col-md-4">
                <div class="learn-more text-center">
                    <a href="{{url('/about-us')}}">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</section>
