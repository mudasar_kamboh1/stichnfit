<section class="testimonial-section">
    <div class="container">
        <div class="testimonial-section-heading">
            <h2 class="text-center">What People Are Saying?</h2>
            <p class="text-center">Our customers singing praises of our work  </p>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="testimonial-slider" class="owl-carousel">
                    <div class="testimonial">
                        <div class="content">
                            <p class="description">
                                "After getting refusals from all the tailors in the Eid season, I decided to give Stich n Fit a try. They not only picked my order but delivered a perfectly stitched dress on time."
                            </p>
                        </div>
                        {{--<div class="testimonial-pic">--}}
                            {{--<img src="{{asset('/assets/img/test-img.png')}}" alt="">--}}
                        {{--</div>--}}
                        <div class="testimonial-review">
                            <h3 class="testimonial-title">Mrs. Anwar</h3>
                            {{--<span>web developer</span>--}}
                        </div>
                    </div>
                    <div class="testimonial">
                        <div class="content">
                            <p class="description">
                            "Online tailor experiences can be a disaster but Stich n Fit completely changed my perception after I gave them a chance on the recommendation of a friend."
                            </p>
                        </div>
                        {{--<div class="testimonial-pic">--}}
                            {{--<img src="{{asset('/assets/img/test-img.png')}}" alt="">--}}
                        {{--</div>--}}
                        <div class="testimonial-review">
                            <h3 class="testimonial-title">Shaheen Shafiq </h3>
                            {{--<span>Web Designer</span>--}}
                        </div>
                    </div>
                    <div class="testimonial">
                        <div class="content">
                            <p class="description">
                                "I tried their party wear stitching service and was amazed to see the end result. Perfect fitting, flawless stitching, Stich n Fit made me the star of the evening in the event."
                            </p>
                        </div>
                        {{--<div class="testimonial-pic">--}}
                            {{--<img src="{{asset('/assets/img/test-img.png')}}" alt="">--}}
                        {{--</div>--}}
                        <div class="testimonial-review">
                            <h3 class="testimonial-title">Zahra Paracha</h3>
                            {{--<span>Web Designer</span>--}}
                        </div>
                    </div>
                    <div class="testimonial">
                        <div class="content">
                            <p class="description">
                                "Wedding preparation in exam days is nothing but a bane. Thanks to Stitch n Fit for being the rescuer and stitching all my dresses perfectly without me having to go to them. Kudos to you! "                  </p>
                        </div>
                        {{--<div class="testimonial-pic">--}}
                            {{--<img src="{{asset('/assets/img/test-img.png')}}" alt="">--}}
                        {{--</div>--}}
                        <div class="testimonial-review">
                            <h3 class="testimonial-title">Ayesha Haider </h3>
                            {{--<span>Web Designer</span>--}}
                        </div>
                    </div>
                    <div class="testimonial">
                        <div class="content">
                            <p class="description">
                                "Surprise wedding function of a friend proved to be a big trouble for me as no one was ready to stitch my dress within a week. I tried Stitch n Fit and they did that in 4 days! Simply WOW. "                  </p>
                        </div>
                        {{--<div class="testimonial-pic">--}}
                            {{--<img src="{{asset('/assets/img/test-img.png')}}" alt="">--}}
                        {{--</div>--}}
                        <div class="testimonial-review">
                            <h3 class="testimonial-title">Amen Jabran</h3>
                            {{--<span>Web Designer</span>--}}
                        </div>
                    </div>
                                   </div>
            </div>
        </div>
    </div>
    </div>
</section>
