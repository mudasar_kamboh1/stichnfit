

<!-- pickup schedule order Modal -->
<div class="modal fade" id="pickup-schedule" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content cstm-modal-content">
            <div class="modal-body">
                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h1>Pickup Schedule</h1>

                    <form action="#" class="order-form" id="schedule-form">
                        @csrf
                        <div id="otp-track-block">
                            <input type="hidden" name="token" id="order-token-no"/>
                            <div class="form-group">
                                <label for="name" class="form-label">Enter Your Pickup Schedule:*</label>
                                <div class="controls input-append date form_datetime " data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1">
                                    <input size="16" class="form-control"  name="pickup_time" type="text" value="" placeholder="Your Pickup Time" autocomplete="off">
                                    <span class="add-on"><i class="icon-remove"></i></span>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <select data-placeholder="Select Your area" class="chosen-select" name="area_id"tabindex="2" id="area">
                                    @foreach(\App\Area::all() as $area)
                                        <option value="{{ $area->id }}">{{ $area->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-submit">Save</button>
                                <button type="button"  class="btn btn-default btn-cancel" data-dismiss="modal" >Cancel</button>
                            </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Call Us Modal -->
<div class="modal fade" id="call-us" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content cstm-modal-content">
            <div class="modal-body">
                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h1>Call Us</h1>

                    <ul>

                        <li><i class="fas fa-phone"></i>&nbsp;Phone : <a href="tel:04232532277">042-32532277</a></li>
                        <li><i class="fab fa-whatsapp"></i>&nbsp;Mobile : <a href="tel:03093333763">0309-3333763</a></li>


                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>


<!-- Track your order Modal -->
<div class="modal fade" id="track" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-sm">
        <div class="modal-content cstm-modal-content">
            <div class="modal-body">

                <div class="order-track-section">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h1>Verify your contact number</h1>

                    <form action="#" class="order-form" id="check-track-otp">
                        @csrf
                        <div id="otp-track-block">
                            <input type="hidden" value="" name="type" id="popup-type"/>
                            <input type="hidden"  name="mobile" id="otp-mobile">
                            <input type="hidden"  name="order" id="order-response" >
                            <div class="form-group">
                                <label for="name" class="form-label">Aapke mobile pe beja howa Password darj kijiye:*</label>
                                <input type="text" class="form-control cstm-control" name="otp" placeholder="Password">
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-submit" id="otpSubmit">Proceed</button>
                                <button type="button"  class="btn btn-default btn-cancel" data-dismiss="modal" >Cancel</button>
                                <br>
                                <span class="countdown"></span>
                                <br>
                                <button type="button"  class="btn btn-default resend-otp" id="resend" disabled="disabled">Resend</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('/assets/js/jquery.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.validate.js') }}"></script>
{{--<script src="{{ asset('/assets/js/jquery.inputmask.bundle.js') }}"></script>--}}

<script type="text/javascript">
    $(document).ready(function() {


        setTimeout(function () {
            $("#type-{{(Request::get('type'))?Request::get('type'):1}}").click();
        },100);

        $("#show").click(function(){
            $("#order-detail").slideToggle("slow");
            if($('.view-detail').text() == 'Hide Detail')
            {
                $('.view-detail').text('View Detail');
            }
            else
            {
                $('.view-detail').text('Hide Detail');
            }
        });
    });
    var $input = $('.num')
    $input.keyup(function(e) {
        var max = 11;
        if ($input.val().length > max) {
            $input.val($input.val().substr(0, max));
        }
    });


    $(window).on('load', function()
    {
        // var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
        // $('#textbox').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // $('#textbox2').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // $('#textbox-mobile').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // $('#textbox2-mobile').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // $('#phone').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // $('#track-mobile').inputmask({
        //     mask: phones,
        //     greedy: false,
        //     definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
        // setTimeout(function() {
        //     $("#resend").show();
        // },  30000);
    });

</script>

<script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('/assets/js/toastr.min.js') }}"></script>
@toastr_render
<script src="{{ asset('/assets/js/sweetalert.js') }}"></script>
<script src="{{ asset('/assets/js/baguetteBox.min.js') }}"></script>

<script>
    $(document).on('submit','#contact-form',function (e) {
        e.preventDefault();
        let loading = '<i class="fas fa-spinner fa-pulse"></i>';
        let $this = $(this);
        $.ajax({
            type: "POST",
            url: "{{url('contact')}}",
            data : $this.serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $this.find('button').html(loading + ' Processing').attr('disabled','disabled');
            },
            success: function(result){
                $this[0].reset();
                $this.find('button').html('Proceed').removeAttr('disabled');
                if(result.status){
                    swal({
                        title: 'Success',
                        text: result.msg ,
                        icon: "success",
                        button: "OK"
                    });
                }else{
                    swal({
                        title: 'Error',
                        text: result.msg ,
                        icon: "error",
                        button: "OK"
                    });
                }
            },
            error: function (request, status, error) {
                $this.find('button').html('Proceed').removeAttr('disabled');

                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    });
    baguetteBox.run('.tz-gallery');
    var mobile;
    $(document).ready(function(){
        $("#testimonial-slider").owlCarousel({
            items:3,
            itemsDesktop:[1000,3],
            itemsDesktopSmall:[990,2],
            itemsTablet:[767,1],
            pagination:true,
            navigation:true,
            autoPlay:true,
            loop:true,

        });
    });
    $("#order-form").validate({
        rules: {
            name:{
                required: true,
            },
            mobile: {
                required: true,
                number:true,
                maxlength:11,
            },
            email: {
                email: true
            },
            address: {
                required: true,
            },
        },
        messages: {
            name:
                {required:"Please enter your name",
                    pattern: "Please enter valid name",
                },
            mobile: {
                required: "Please enter a valid number",
                maxlength: "Please enter at least 11 Characters",
            },
            email: "Please enter a valid email address",
        }
    });
    $("#contact-form").validate({
        rules: {
            name:{
                required: true,
            },
            mobile: {
                required: true,
                maxlength:11,
                number: true,
            },
            email: {
                email: true
            },
            address: {
                required: true,
            },
        },
        messages: {
            name:
                {required:"This filed is required",
                    pattern: "Please enter valid name",
                },
            mobile: {
                required: "This filed is required",
                maxlength: "Mobile number consist of numeric characters",
                number: "Please enter a valid number"
            },
            email: "Please enter a valid email address",
        }

    });
    $("#mobile-form").validate({
        rules: {
            mobile: {
                required: true,
                maxlength:11,
                number: true,
            },
        },
        messages: {
            mobile: {
                required: "This filed is required",
                maxlength: "Mobile number consist of numeric characters",
                number: "Please enter a valid number"
            },
        }
    });
    $('.order-detail').on('click',function () {
        getData($(this).data('order-id'));
    });
    $(document).on('submit','#send-track-otp',function (e) {
        e.preventDefault();
        $('#otp-mobile').val($('#mobile-number3').val());
        $('#popup-type').val('track');
        let $this = $(this);
        sendOtp($this);
    });
    $(document).on('submit','#send-order-otp',function (e) {
        e.preventDefault();
        $('#otp-mobile').val($('#mobile-number2').val());
        $('#popup-type').val('my-orders');
        let $this = $(this);
        sendOtp($this);
    });
    $(document).on('submit','#order-form',function (e) {
        e.preventDefault();
        let $this = $(this);
        $('#otp-mobile').val($('#mobile-number').val());
        $('#order-response').val(1);
        let loading = '<i class="fas fa-spinner fa-pulse"></i>';
        $.ajax({
            url: "{{url('order')}}",
            type: "POST",
            data : $this.serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $this.find('button.order-btn').html(loading + ' Processing').attr('disabled','disabled');
            },
            success: function(result){
                $this[0].reset();
                $this.find('button.order-btn').html('Proceed').removeAttr('disabled');
                if(result.status){
                    toastr.success(result.msg);
                    $('#track').modal('show');
                    startCountDown();
                }else{
                    toastr.error(result.msg);
                }
            },
            error: function (request, status, error) {
                $this.find('button.order-btn').html('Proceed').removeAttr('disabled');
                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    });
    $(document).on('click','.resend-otp',function (e) {
        e.preventDefault();
        var mobile = $('#otp-mobile').val();
        if(mobile != ''){
            $.ajax({
                url: "{{url('send-track-otp')}}",
                type: "GET",
                data : {'mobile':mobile},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function(result){
                    startCountDown();
                    if(result.status){
                        toastr.success(result.msg);
                    }else{
                        toastr.error(result.msg);
                    }
                },
                error: function (request, status, error) {
                    $this.find('#otpSubmit').html('Proceed').removeAttr('disabled');
                    let json = $.parseJSON(request.responseText);
                    $.each(json.errors, function(key, value){
                        toastr.error(value);
                    });
                }
            });
        }else{
            toastr.error('Something went wrong!');
        }
    });
    $(document).on('submit','#check-track-otp',function (e) {
        e.preventDefault();
        let loading = '<i class="fas fa-spinner fa-pulse"></i>';
        let $this = $(this);
        $.ajax({
            url: "{{url('check-track-otp')}}",
            type: "GET",
            data : $this.serialize()+ '&type=' + $('#popup-type').val(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $this.find('#otpSubmit').html(loading + ' Processing').attr('disabled','disabled');
            },
            success: function(result){
                $this.find('#otpSubmit').html('Proceed').removeAttr('disabled');
                if(result.status){
                    $('#track').modal('hide');
                    swal({
                        title: 'Success',
                        text: result.msg ,
                        icon: "success",
                        button: "OK"
                    }).then(function () {
                        location.href = "{{route('myOrders')}}";
                    });

                    {{--setTimeout(function () {--}}
                        {{--location.href = "{{route('myOrders')}}";--}}
                    {{--},1000);--}}
                }else{
                    swal({
                        title: 'Error',
                        text: result.msg ,
                        icon: "error",
                        button: "OK"
                    });
                }
            },
            error: function (request, status, error) {
                $this.find('#otpSubmit').html('Proceed').removeAttr('disabled');
                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    });

    function startCountDown() {
        $('#resend').attr('disabled','disabled');
        var minutes = 0, seconds = 59;
        $("span.countdown").html(minutes + ":" + seconds);
        var count = setInterval(
        function(){
            if(parseInt(minutes) < 0) {
                clearInterval(count);
            } else {
                $("span.countdown").html("00 :" + seconds);
                if(seconds == 0) {
                    $('#resend').removeAttr('disabled');
                    clearInterval(count);
                }
                seconds--;
                if(seconds < 10) seconds = "0"+seconds;
            }
        }, 1000);
    }

    function sendOtp($this){
        let loading = '<i class="fas fa-spinner fa-pulse"></i>';
        $.ajax({
            url: "{{url('send-track-otp')}}",
            type: "GET",
            data : $this.serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $this.find('button').html(loading + ' Processing').attr('disabled','disabled');
            },
            success: function(result){
                $this.find('button').html('Proceed').removeAttr('disabled');
                if(result.status){
                    if(result.redirect !== null){
                        toastr.info('Fetching Orders...');
                        location.href = result.redirect;
                    }else{
                        toastr.success(result.msg);
                        startCountDown();
                        $('#my-orders').modal('hide');
                        $('#track').modal('show');
                    }
                }else{
                    swal({
                        title: 'Error',
                        text: result.msg ,
                        icon: "error",
                        button: "OK"
                    });
                }
            },
            error: function (request, status, error) {
                $this.find('button').html('Proceed').removeAttr('disabled');
                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    }
    $('.set-pickup-schedule').on('click',function () {
        $('#order-token-no').val($(this).data('token'));
        $('#pickup-schedule').modal('show');
    });
    $('#schedule-form').on('submit',function (e) {
        e.preventDefault();
        let loading = '<i class="fas fa-spinner fa-pulse"></i>';
        let $this = $(this);
        $.ajax({
            url: "{{url('update-pickup-time')}}",
            type: "POST",
            data : $this.serialize(),
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $this.find('button.order-submit').html(loading + ' Processing').attr('disabled','disabled');
            },
            success: function(result){
                $this.find('button.order-submit').html('Proceed').removeAttr('disabled');
                if(result.status){
                    toastr.success(result.msg);
                    location.reload();
                }else{
                    toastr.error(result.msg);
                }
            },
            error: function (request, status, error) {
                $this.find('button.order-submit').html('Proceed').removeAttr('disabled');
                let json = $.parseJSON(request.responseText);
                $.each(json.errors, function(key, value){
                    toastr.error(value);
                });
            }
        });
    });

    function getId(str)
    {
        return str.replace(/[^0-9.]/g,'');
    }


</script>

{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>--}}
{{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>--}}
{{--<script type="text/javascript">--}}
    {{--$(function () {--}}
        {{--// $('#datetimepicker1').datetimepicker();--}}
        {{--$('#datetimepicker1').datetimepicker({ minDate: new Date()});--}}
    {{--});--}}
{{--</script>--}}
<script src="{{ asset('/assets/js/bootstrap-datetimepicker.js') }}"></script>
{{--<script src="{{ asset('/assets/js/bootstrap-datetimepicker.fr.js') }}"></script>--}}
<script>
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        startDate: '-0d',
        todayBtn:  1,
        format: 'dd-mm-yyyy H:iiP',
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
</script>
<script src="{{ asset('/assets/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('/assets/js/prism.js') }}"></script>
<script src="{{ asset('/assets/js/init.js') }}"></script>
