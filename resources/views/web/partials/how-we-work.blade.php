<section class="how-we-work-main">
    <div class="container">
        <h1 class="text-center">How We Work</h1>
    <div class="col-md-12">
        <div class="how-we-work-heading">
            <h4>Your perfect fit starts here</h4><span></span>
        </div>
    <div class="how-we-work-img mobile-hide">
        <img src="{{asset('/assets/img/how-we-work.png')}}" width="100%" alt="how we work">
    </div>
        <div class="how-we-work-img web-hide">
            <img src="{{asset('/assets/img/portrait.png')}}" width="100%" alt="how we work">
        </div>
    </div>
    </div>
</section>
