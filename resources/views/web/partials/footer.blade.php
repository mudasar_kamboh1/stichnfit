<section class="footer-section" style="background-image: url({{asset('/assets/img/footer-image.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer-content">
                    <h2>About us</h2>
                    <p class="underline"></p>
                    <p class="text-justify">Stitch N Fit – your one-stop stitching solution –
                        is stitching your style with complete elegance
                        and perfection! Get in touch with us for all your
                        queries. </p>
                    <div class="social-media-icons ss">
                        <ul>
                            <li class="fb"><a href="https://www.facebook.com/stitchnfitworld" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li class="twit"><a href="https://www.twitter.com/stitchnfitworld" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li class="inst"><a href="https://www.instagram.com/stitchnfitworld" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 ">
                <div class="footer-content link">
                    <h2>Customer Care</h2>
                   <p class="underline"></p>
                    <ul>
                        <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                        <li><a href="{{url('/faq')}}"> FAQ's</a></li>
                       <li><a href="{{url('/privacy-policy')}}">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <div class="footer-content link">
                    <h2>Talk To Us</h2>
                    <p class="underline"></p>
                    <ul>
                        <li>Phone : <a href="tel:04232532277">042-32532277</a></li>
                        <li>Mobile : <a href="tel:03093333763">0309-3333763</a></li>
                        <li>Email : <a href="mailto:info@stitchnfit.com">info@stitchnfit.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="copyright-section">
    <div class="container text-center">
    <p>© 2018 Stitch N Fit- All Rights Reserved </p>
    </div>
</section>

{{--@push('scripts')--}}
    {{--<script src="{{ asset('/assets/js/jquery.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/js/jquery.validate.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/js/jquery.inputmask.bundle.js') }}"></script>--}}
    {{--<script>--}}
        {{--$(window).on('load', function()--}}
        {{--{--}}
            {{--var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];--}}
            {{--$('#textbox').inputmask({--}}
                {{--mask: phones,--}}
                {{--greedy: false,--}}
                {{--definitions: { '#': { validator: "[0-9]", cardinality: 1}} });--}}
            {{--$('#textbox2').inputmask({--}}
                {{--mask: phones,--}}
                {{--greedy: false,--}}
                {{--definitions: { '#': { validator: "[0-9]", cardinality: 1}} });--}}
            {{--$('#track-mobile').inputmask({--}}
                {{--mask: phones,--}}
                {{--greedy: false,--}}
                {{--definitions: { '#': { validator: "[0-9]", cardinality: 1}} });--}}
        {{--});--}}

    {{--</script>--}}

    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function() {--}}
            {{--setTimeout(function () {--}}
                {{--$("#type-{{(Request::get('type'))?Request::get('type'):1}}").click();--}}
            {{--},100);--}}

            {{--$("#type-3").click(function () {--}}
                {{--$(".measurements-block").show();--}}
                {{--$(".old-measurements").hide();--}}


            {{--});--}}
            {{--$("#type-1").click(function () {--}}
                {{--$(".measurements-block").hide();--}}
                {{--$(".old-measurements").hide();--}}

            {{--});--}}
            {{--$("#show").click(function(){--}}
                {{--$("#order-detail").slideToggle("slow");--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    {{--<script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/js/toastr.min.js') }}"></script>--}}
    {{--@toastr_render--}}

    {{--<script src="{{ asset('/assets/js/sweetalert.js') }}"></script>--}}

{{--@endpush--}}
