<!-- gallery section start here -->
<section class="gallery-section">
    <div class="container">
        <div class="gellery-section-heading">
            <h2 class="text-center">Our Gallery</h2>
            <p class="text-center">A Glimpse of What We Have Stitched Previously </p>
        </div>
        <div class="row">
            <div class="col-md-4 padd-rl-5">
                <div class="row margin-auto">
                    @if(count($posts) > 0)
                        @foreach($posts as $gallery)
                        <div class="col-sm-6 padd-5">
                            <div class="gallery-avatar">
                                <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}">
                                    <img src="{{asset('/assets/uploads/'.$gallery->image)}}" width="100%" alt=""></a>
                                <div class="gallery-heading text-center">
                                    <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}">{{ $gallery->name }}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    @endif
            </div>
        </div>
            <div class="col-md-4 padd-rl-5">
                <div class="main-avatar">
                    <img src="{{asset('/assets/img/middle-img.png')}}" width="100%" alt="">
                </div>
            </div>
            <div class="col-md-4 padd-rl-5">
                <div class="row margin-auto">
                    @if(count($posts1) > 0)
                        @foreach($posts1 as $gallery)
                            <div class="col-sm-6 padd-5">
                                <div class="gallery-avatar">
                                    <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}">
                                        <img src="{{asset('/assets/uploads/'.$gallery->image)}}" width="100%" alt=""></a>
                                    <div class="gallery-heading text-center">
                                        <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}">{{ $gallery->name }}</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-12 text-center view-more">
                <a href="{{url('/gallery')}}" class="view-more-btn">View More</a>
            </div>
    </div>
    </div>
</section>
