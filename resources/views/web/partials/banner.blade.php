<section class="banner-section">
    {{--<div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">--}}
        {{--<div class="carousel-inner">--}}
            {{--<div class="carousel-item active">--}}
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-A.jpg')}}" alt="first banner">--}}
            {{--</div>--}}
            {{--<div class="carousel-item">--}}
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-B.jpg')}}" alt="second banner">--}}
            {{--</div>--}}
            {{--<div class="carousel-item">--}}
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-C.jpg')}}" alt="third banner">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url({{asset('/assets/img/BANNER-A.jpg')}});">
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-A.jpg')}}" alt="first banner">--}}
            </div>
            <div class="carousel-item" style="background-image: url({{asset('/assets/img/BANNER-B.jpg')}});">
{{--                <img class="d-block w-100" src="{{asset('/assets/img/BANNER-B.jpg')}}" alt="second banner">--}}
            </div>
            <div class="carousel-item" style="background-image: url({{asset('/assets/img/BANNER-C.jpg')}});">
{{--                <img class="d-block w-100" src="{{asset('/assets/img/BANNER-C.jpg')}}" alt="third banner">--}}
            </div>
        </div>
    </div>
    <div id="carousel-tab" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000" style="display:none;">
        <div class="carousel-inner">
            <div class="carousel-item active"  style="background-image: url({{asset('/assets/img/BANNER-A-Recovered.jpg')}});">
{{--                <img class="d-block w-100" src="{{asset('/assets/img/BANNER-A-Recovered.jpg')}}" alt="first banner">--}}
            </div>
            <div class="carousel-item"  style="background-image: url({{asset('/assets/img/BANNER-A-Recovered-2.jpg')}});">
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-A-Recovered-2.jpg')}}" alt="first banner">--}}
            </div>
            <div class="carousel-item"  style="background-image: url({{asset('/assets/img/BANNER-A-Recovered-3.jpg')}});">
                {{--<img class="d-block w-100" src="{{asset('/assets/img/BANNER-A-Recovered-3.jpg')}}" alt="first banner">--}}
            </div>
           </div>
    </div>
    <div id="carousel-mobile" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000" style="display:none;">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('/assets/img/mob-1.jpg')}}" alt="first banner">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('/assets/img/mob-2.jpg')}}" alt="second banner">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="{{asset('/assets/img/mob-3.jpg')}}" alt="third banner">
            </div>
        </div>
    </div>
    <div class="container2">
        <div class="col-md-4 cstm-padd">
            @if(!\Illuminate\Support\Facades\Auth::check())
            <div class="order-section">
                <h1>BOOK YOUR ORDER NOW</h1>
                <form action="{{url('order')}}" method="POST" class="order-form" id="order-form">
                    @csrf
                    <div class="form-group">
                        <label for="name" class="form-label">Name:*</label>
                        <input type="text" class="form-control cstm-control" name="name" maxlength="50" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <label for="mobile" class="form-label">Mobile No:*</label>
                        <input type="text" id="textbox"  class="form-control cstm-control num" placeholder="Mobile" name="mobile" onchange="$('#mobile-number').val(getId($('#textbox').val()))"  minlength="11" maxlength="11">
                        <input type="hidden"  id="mobile-number" name="mobile">
                    </div>
                    <div class="form-group">
                        <label for="email" class="form-label">Email:</label>
                        <input type="email" class="form-control cstm-control" name="email" placeholder="Email" maxlength="50">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-default order-btn"><span id="loader"></span>Proceed</button>
                    </div>
                </form>
            </div>
                <div class="order-track-section">
                    <h1>Track Your Order</h1>
                    <form action="#" class="order-form" id="send-track-otp">
                        @csrf
                        <div id="contact-block">
                            <div class="form-group">
                                <label for="name" class="form-label">Mobile:*</label>
                                <input type="text" id="textbox2" placeholder="Mobile" class="form-control cstm-control" maxlength="11" minlength="11"  onchange="$('#mobile-number3').val(getId($('#textbox2').val()))" name="mobile" required="required">
                                <input type="hidden"  id="mobile-number3" name="mobile">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-default order-btn"><span id="track-order-loader"></span>Proceed</button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
        </div>
    </div>
</section>
