<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.order-nav')
@yield('content')
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
{{--@yield('scripts')--}}
</body>
</html>
