@extends('web.other-layout')
@section('content')
<section class="order-track-detail-section">
    <div class="container">
        <div class="order-track-info">
            <div class="order-step-form">
                <div class="col-lg-12 col-sm-12">
                    <form action="" method="POST"  id="order-form" class="order-form">
                        @csrf
                        <h1>BOOK YOUR ORDER NOW</h1>
                        <div class="form-group">
                            <label for="contact">Select One Option:</label>
                        </div>
                        <div class="row no-gutters">
                            <!-- RADIO BUTTONS BLOCK -->
                            <div class="col-md-4">
                                <div class="frb-group">
                                    <div class="frb frb-default text-center">
                                        <input type="radio" id="type-1" name="type" value="1" {{(old('type') == 1 || Request::get('type') == 1)? 'checked':'checked'}}>
                                        <label for="type-1">
                                            <span class="frb-title">Tailor Visit</span>
                                            <span class="frb-description">Select this option if you want our tailor to visit your home for measurements. </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="frb-group">
                                    <div class="frb frb-default old-measurement text-center" data-toggle="modal" data-target="#contact">
                                        <input type="radio" id="type-2" name="type" value="2"{{(old('type') == 2 || Request::get('type') == 2)? 'checked':''}}>
                                        <label for="type-2">
                                            <span class="frb-title">Old Measurements</span>
                                            <span class="frb-description">Select this option if you want to choose Your Existing Measurements.</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="frb-group">
                                    <div class="frb frb-default text-center">
                                        <input type="radio" id="type-3" name="type" value="3" {{(old('type') == 3 || Request::get('type') == 3)? 'checked':''}}>
                                        <label for="type-3">
                                            <span class="frb-title">New Measurements </span>
                                            <span class="frb-description">Select this option if you want to provide the new measurements on your own. </span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control cstm-control" id="name" name="name" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Mobile:</label>
                                    <input type="text" class="form-control cstm-control"  id="mobile" name="mobile" value="{{old('mobile')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="email" class="form-control cstm-control" id="email" name="email" value="{{old('email')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="address">Address:</label>
                                    <input type="text" class="form-control cstm-control"  id="address" name="address" value="{{old('address')}}">
                                </div>
                            </div>
                        </div>
<div class="old-measurements card" id="old-measurements" style="display: none;">
    <h2>Please Select Your existing measurements in Inches</h2>
    <div class="table-responsive">
    <table id="table" class="table table-bordered">
        <thead>
        <tr>
            <th>Shoulder </th>
            <th>Chest </th>
            <th>Waist </th>
            <th>Shirt length </th>
            <th>Trouser length </th>
            <th>Detail </th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Shoulder: 20 </td>
            <td>Chest: 40 </td>
            <td>Waist: 34 </td>
            <td>Shirt length: 54 </td>
            <td>Trouser length: 40 </td>
            <td><button  type="button" class="btn view-detail" data-toggle="modal" data-target="#measurementModal">View Detail</button> </td>
            <td><input type="radio" class="option-input radio" name="radioGroup"></td>
        </tr>
        <tr>
            <td>Shoulder: 20 </td>
            <td>Chest: 40 </td>
            <td>Waist: 34 </td>
            <td>Shirt length: 54 </td>
            <td>Trouser length: 40 </td>
            <td><button class="btn view-detail" type="button" data-toggle="modal" data-target="#measurementModal">View Detail</button> </td>
            <td><input type="radio" class="option-input radio" name="radioGroup"></td>
        </tr>
        <tr>
            <td>Shoulder: 20 </td>
            <td>Chest: 40 </td>
            <td>Waist: 34 </td>
            <td>Shirt length: 54 </td>
            <td>Trouser length: 40 </td>
            <td><button class="btn view-detail"  type="button" data-toggle="modal" data-target="#measurementModal">View Detail</button> </td>
            <td><input type="radio" class="option-input radio" name="radioGroup"></td>
        </tr>
        </tbody>
    </table>
    </div>
</div>
                        <div class="measurements-block" id="measurements">
                            <h2>Add your new measurements in Inches</h2>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="contact">Shoulder:</label>
                                        <input type="text" class="form-control cstm-control" name="shoulder">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Chest:</label>
                                        <input type="text" class="form-control cstm-control" name="chest">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Waist:</label>
                                        <input type="text" class="form-control cstm-control" name="waist">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Shirt length:</label>
                                        <input type="text" class="form-control cstm-control" name="kameez_length">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="contact">Trouser length:</label>
                                        <input type="text" class="form-control cstm-control" name="trouser_length">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Arms length:</label>
                                        <input type="text" class="form-control cstm-control" name="arms_length">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="name">Arms width:</label>
                                        <input type="text" class="form-control cstm-control" name="arms_width">
                                    </div>
                                </div>
                                <!-- <div class="col-md-3">
                                  <div class="form-group">
                                    <label for="name">Name:</label>
                                    <input type="text" class="form-control cstm-control" name="name">
                                  </div>
                                </div> -->
                            </div>
                        </div>
                        <div class="text-left">
                            <button type="submit" class="btn btn-default btn-proceed" value="Validate">proceed</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal fade" id="contact" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="order-track-section">
                                <h1>Verify your Contact Number</h1>
                                <form action="#" class="order-form">
                                    <div id="contact-block">
                                    <div class="form-group">
                                        <label for="name">Contact:</label>
                                        <input type="text" class="form-control cstm-control" name="code" placeholder="Enter your Contact Number">
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default order-btn">Track</button>
                                    </div>
                                    </div>
                                    <div id="otp-block" style="display: none;">
                                    <div class="form-group">
                                        <label for="name">Enter OTP Code:</label>
                                        <input type="text" class="form-control cstm-control" name="otp" placeholder="Enter your OTP Code">
                                    </div>
                                    <div class="text-center">
                                        <button type="button" class="btn btn-default order-submit">submit</button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="measurementModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="container cstm-container">
                    <div class="col-md-12 submit-order-detail">
                        <div class="row padd-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Token No.</label>&nbsp;<span>abc</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Name:</label>&nbsp;<span>Not Available</span>
                                </div>
                            </div>
                        </div>
                        <div class="row padd-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Mobile:</label>&nbsp;<span>Not Available</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Email:</label>&nbsp;<span>Not Available</span>
                                </div>
                            </div>
                        </div>
                        <div class="row padd-20">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Type:</label>&nbsp;<span>Not Available</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="contact">Status:</label>&nbsp;<span>Not Available</span>
                                </div>
                            </div>
                        </div>
                        <div class="row padd-20">
                            <div class="col-md-2" style="padding-right: 0px;">
                                <div class="form-group">
                                    <label for="contact" style="width:100%;">Address:</label>&nbsp;
                                </div>
                            </div>
                            <div class="col-md-10" style="padding-left: 3px;">
                                <div class="form-group">
                                    <span style="width:100%; display:block;">Not Available</span>
                                </div>
                            </div>
                        </div>
                        <div class="measurement-section">
                            <h2>Measurement</h2>
                            <div class="row padd-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Shoulder:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Chest:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row padd-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">waist:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row padd-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Trouser length:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Kameez Length:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row padd-20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Arm width:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="contact">Arm length:</label>&nbsp;<span>Not Available</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
@endpush
