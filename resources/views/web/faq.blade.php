<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit - Frequently Asked Questions | Tailor-made Sewing and Alteration Services</title>
    <meta charset="UTF-8">
    <meta name="description" content="Stitch n Fit offers sewing and alteration services tailor-made to your requirements and sewed to perfection. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="online stitching, stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--FAQ banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/faq.png')}}" alt="Frequently-asked-questions">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">FAQ</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--FAQ description section--}}
<section class="how-we-work-section">
    {{--<div class="container">--}}
        {{--<div class="col-md-12 text-left">--}}
        {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container">
    <div class="row no-gutters">
        <div class="col-md-12">
            <p class="bold">What is Stitch n Fit all about?</p>
            <p class="text-justify">Stitch n Fit is a premium online stitching services provider. We are offering sewing and alteration services allowing you to get your dresses stitched and altered without having to go out of your house. No matter if it’s formal wear, party wear, bridal dresses, or just casual kurti, we will stitch your dress that will fit you perfectly.</p>
            <p class="bold">About the team of Stitch n Fit.</p>
            <p class="text-justify">Stitch n Fit is a team of highly experienced and skillful tailor masters who have years of experience in this field. The team consists of professional tailors, fashion designers, and strict quality control team who ensure that the dress is a proper depiction of your dreams.</p>
            <p class="bold">Why Choose Stitch n Fit?</p>
            <p class="text-justify">Stitch n Fit is the right solution to all those frustrations and delays that you have to go through while dealing with your conventional tailor. Life is fast and so are we! We offer a convenient way to get all your dresses stitched and altered by professional tailor experts from the comfort of your home. You just have to book your order and we will pick it up at your prescribed time and have it delivered to you once it is stitched. </p>
            <p class="bold">What type of dresses do you stitch?</p>
            <p class="text-justify">Stitch n Fit takes pride in converting any type of fabric into a beautiful looking stitched dress. One piece, two-piece, three-piece, bridal wear, party wear, formal wear, shalwar suit, or whatever – you name it, we sew it!</p>
            <p class="bold">What are the sizes?</p>
            <p class="text-justify">From the standard small, medium, large, and extra large sizes to customized sizes, we do it all with sheer accuracy.</p>
            <p class="bold">What are the minimum order requirements for pick up and delivery service?</p>
            <p class="text-justify">Stitch n Fit firmly believes in Quaid’s motto of “Work, Work, and only Work”. We don’t have any minimum order requirements for order pick up and delivery.</p>
            <p class="bold">What if the dress does not fit to perfection? </p>
            <p class="text-justify">if you have provided us with the right garment for reference that fits you, you can be sure that your dress will not have any issue. However, if you still believe that your dress is not correctly stitched, you can contact us and we will try to resolve the issue at the earliest.</p>
            <p class="bold">How to place an order? </p>
            <p class="text-justify">To place an order, you just need to fill out the form and schedule your pick up time. Our team will visit you for the fabric collection and your order will be confirmed.</p>
        </div>
    </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')

@include('web.partials.assets')

</body>
</html>
