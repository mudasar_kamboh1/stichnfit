<!DOCTYPE html>
<html lang="en">
    @section('metas')
        <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
        <meta charset="UTF-8">
        <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
        <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
    @endsection
    {{--// Including Head Section--}}
    @include('web.partials.head')
<body>
    {{--// Including Nav and Slide Section--}}
    @include('web.partials.nav-slide')
    {{--// Including Services Section--}}
    @include('web.partials.banner')
    {{--// Including how we work Section--}}
    @include('web.partials.how-we-work')
    {{--// Including Services Section--}}
    @include('web.partials.services')
    {{--// Including what we stitch Section--}}
    @include('web.partials.stitch')
 {{--// Including gallery Section--}}
    @include('web.partials.gallery')
    {{--// Including NewsSection--}}
    @include('web.partials.news')
    {{--// Including NewsSection--}}
    @include('web.partials.testimonials')
    {{--// Including Footer Section--}}
    @include('web.partials.footer')
    {{--// Including assets --}}
    @include('web.partials.assets')
</body>
</html>
