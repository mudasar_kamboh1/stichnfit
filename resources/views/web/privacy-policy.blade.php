<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--privacy policy banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/privacy-policy.png')}}" alt="privacy-policy">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">Privacy Policy</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--privacy policy description section--}}
<section class="how-we-work-section">
    {{--<div class="container">--}}
        {{--<div class="col-md-12">--}}
            {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container">
    <div class="row no-gutters">
        <div class="col-md-12">
<p></p>
<p></p>
            <p>We, at Stitch n Fit, value our customers and this is the reason why we are very clear about our policies. These policies have been designed in the best interest of the customers/clients. </p><br>
            <p class="bold">Free Pickup & Delivery</p>
            <p class="text-justify">Stitch n Fit offers free pickup and delivery for all types of stitching and alteration services without any limitation of minimum order. Please note that this policy is subject to change without prior notice.</p>
            <p class="bold">Issues with Size & Design</p>
            <p class="text-justify">Our expert tailors ensure that the tailoring job is done perfectly as per the design and size provided by the customers. In case of any dispute in size or design, if the fault is from our end, we will compensate you and sew the dress for free. If the issue is from the customer’s end, no compensation will be provided whatsoever. </p>
            <p class="bold">Delivery Time</p>
            <p class="text-justify">Stitch n Fit offers swift response and the quickest delivery time of just 3 working days for all normal wear suits. For wedding dresses, the delivery time may vary according to the nature of the job. Our team of experienced tailors has the speed and accuracy to complete the job within the prescribed time.</p>
            <p class="bold">No Cancellations  </p>
            <p class="text-justify">We are a group of workaholics who believe that our work is our driving force. This drive to work makes us accept all the orders. We don’t cancel any order after acceptance. Once we pick up a challenge, we give our heart and soul to it.</p>
        </div>
    </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
