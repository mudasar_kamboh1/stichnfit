@extends('web.other-layout')
@section('content')
    <section class="inner-banner-section" style="background:#ebebeb;">
        <div class="carousel">
            <div class="carousel-inner">
                <div class="carousel-item2 active">
                    <img class="d-block w-100" src="{{asset('assets/img/about-banner.png')}}" alt="My order">
                    <div class="carousel-caption cstm-caption">
                        <div class="col-md-12">
                            <h1 class="text-center">My Orders</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <div class="order-track-detail-section">
     {{--<div class="container">--}}
         {{--<div class="col-md-12">--}}
             {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
         {{--</div>--}}
     {{--</div>--}}
     <div class="container">
<div class="row">
    <div class="col-md-8">
             <div class="order-heading">
                 <p>My Orders <span class="order-note">View all your pending, delivered and returned orders here.</span></p>
         </div>
    </div>
    <div class="col-md-4">
        <div class="create-new">
            <a onclick="return confirm('Are you sure to proceed new order?');" href="{{ url('new-order') }}">Create New Order</a>
        </div>
    </div>
</div>
         <div class="col-md-12">
         <div class="order-track-info">
             <div class="row margin-auto">
                 <div class="col-md-6">
                     <div class="profile">
                         <div class="media">
                             <div class="media-left">
                                 <img src="{{asset('assets/img/profile.jpg')}}" class="media-object" width="75%" alt="">
                             </div>
                             <div class="media-body">
                                 <h4 class="media-heading">{{ $user->name }}</h4>
                                 <a href="profile">Update Profile </a>
                             </div>
                         </div>
                     </div>
                 </div>
                 <div class="col-md-6">
                     <div class="call-to-us text-right">
                         <a href="#" data-toggle="modal" data-target="#call-us">Call Us</a>
                     </div>
                 </div>
                 <div class="order-track-detail col-md-12 mobile-hide">
                     <div class="col-lg-12 col-sm-12">
                         <div class="table-responsive">
                             <table class="table table-borderless">
                                 @if(count($orders) > 0)
                                 @foreach($orders as $order)
                                 <tr>
                                     <td>
                                         @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                                             <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="100" height="100">
                                         @else
                                             <img src="{{asset('assets/img/download.jpg')}}" alt="Order Image" title="" width="100" height="100">
                                         @endif
                                     </td>
                                     <td>
                                         <span class="track-order-status-heading">Token No.</span>
                                         <br>
                                         <span class="track-order-name">{{ !is_null($order)? config('constants.order_token_prefix').$order->token : '' }}</span><br>
                                     </td>
                                     <td><span class="track-order-status-heading">Status:</span><br>
                                         <span class="In-process">{{ config('constants.order_status.'.$order->status) }}</span></td>
                                     <td><span class="delivery-status">Made On:</span><br>
                                         <span class="delivery-date">{{ !is_null($order) && $order->created_at ? \Carbon\Carbon::parse($order->created_at)->format('d, M, Y h:i A') : '-- -- --' }}</span></td>
                                     <td><span class="track-order-status-heading">Pickup Schedule</span><br>
                                         <span class="delivery-date">{{ !is_null($order) && $order->pickup_time? \Carbon\Carbon::parse($order->pickup_time)->format('d, M, Y h:i A') : '-- -- --' }}</span></td>
                                     <td><span class="delivery-status">Location:</span><br>
                                         <span class="delivery-date">{{ !is_null($order) && !is_null($order->area) ? $order->area->name: $order->user->address }}</span></td>
                                     <td class="text-center">
                                         <a class="track-order" href="{{ url('order-detail/'.$order->token) }}">view Order</a>
                                         <a class="track-order set-pickup-schedule" data-token="{{ $order->token }}">{{ is_null($order->pickup_time) ? 'Set Pickup Schedule' : 'Edit Pickup Schedule' }}</a></td>
                                 </tr>
                                     @endforeach
                                     @else
                                         <tr>
                                         <td><br>
                                             <h3 class="text-center" style="display: table; margin: 0 auto;color: red;">No Order To Show</h3>
                                         </td>
                                         </tr>
                                     @endif
                                 </tbody>
                             </table>
                         </div>
                     </div>
                     <br>
                     <div class="content-center">
                         {{$orders->links( "pagination::bootstrap-4")}}
                     </div>
               </div>
                 <div class="order-track-detail col-md-12 web-hide">
                     <div class="mobile-card">
                         <div class="row">
                             @if(count($orders) > 0)
                                 @foreach($orders as $order)
                                     <div class="col-md-3">
                                         <div class="card mb-4">
                                             @if($order->image && file_exists(public_path().'/assets/uploads/orders/'.$order->image))
                                                 <img src="{{ asset('/assets/uploads/orders/'.$order->image ) }}" alt="Order Image" title="" width="100%" height="170">
                                             @else
                                                 <img src="{{asset('assets/img/download.jpg')}}" alt="Order Image" title="" width="100%" height="170">
                                             @endif
                                             {{--<img class="card-img-top" src="https://placeimg.com/290/180/any" alt="Card image cap">--}}
                                             <div class="card-body">
                                                 <span class="track-order-status-heading">Token No.</span>
                                                 <span class="track-order-name">{{ !is_null($order)? config('constants.order_token_prefix').$order->token : '' }}</span><br>
                                                 <span class="track-order-status-heading">Status:</span>
                                                 <span class="In-process">{{ config('constants.order_status.'.$order->status) }}</span><br><br>
                                                 <a class="track-order" href="{{ url('order-detail/'.$order->token) }}">view Order</a>
                                                 <a class="track-order set-pickup-schedule" data-token="{{ $order->token }}">{{ is_null($order->pickup_time) ? 'Set Pickup Schedule' : 'Edit Pickup Schedule' }}</a>                                     </div>
                                         </div>
                                     </div>
                                 @endforeach
                            @else
                                  <h3 class="text-center" style="display: table; margin: 0 auto;color: red;"><br>No Order To Show</h3>
                            @endif
                         </div>
                     </div>
                     <br>
                     <div class="content-center">
                         {{$orders->links( "pagination::bootstrap-4")}}
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
     <section class="inner-banner-section mobile-hide" style="background:#ebebeb;">
               <div class="carousel">
             <div class="carousel-inner">
                 <div class="carousel-item2 active">
                     <img class="d-block w-100" src="{{asset('assets/img/ratelist-landscap.jpg')}}" alt="Price List">
                 </div>
             </div>
         </div>
       </section>
     <section class="inner-banner-section web-hide" style="background:#ebebeb;">
               <div class="container">
                     <img class="d-block w-100" src="{{asset('assets/img/ratelist-potrait.jpg')}}" alt="Price List">

         </div>
       </section>
 </div>
@endsection
