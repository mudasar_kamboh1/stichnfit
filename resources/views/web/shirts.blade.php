<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--Shirt gallery banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/gallery.png')}}" alt="">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">Gallery</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="single-gallery-section">
    {{--<div class="container">--}}
        {{--<div class="col-md-12">--}}
            {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container">
        <div class="gallery-items">
            <div class="row">
                @if(count($posts) > 0)
                    @foreach($posts as $gallery)
                    <!-- gallery items-->
                    <div class="col-md-3">
                        <div class="card cstm-card">
                           <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}"> <img class="card-img-top" src="{{asset('/assets/uploads/'.$gallery->image)}}" width="100%" alt=""></a>
                            <div class="card-body">
                                <h5 class="card-title">{{ $gallery->name }}</h5>
                                <p>
                                    {!! \Illuminate\Support\Str::words($gallery->description, $words = 22, $end = '...') !!}
                                </p>
                                <a href="{{ url('gallery/'. $gallery->sub_category->slug.'/'.$gallery->slug) }}" class="btn view-btn">View Now</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
