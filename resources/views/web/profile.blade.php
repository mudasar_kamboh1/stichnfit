@extends('web.other-layout')
@section('content')
    <section class="inner-banner-section" style="background:#ebebeb;">
        <div class="carousel">
            <div class="carousel-inner">
                <div class="carousel-item2 active">
                    <img class="d-block w-100" src="{{asset('assets/img/about-banner.png')}}" alt="">
                    <div class="carousel-caption cstm-caption">
                        <div class="col-md-12">
                            <h1 class="text-center">Profile</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="order-track-detail-section">
        {{--<div class="container">--}}
            {{--<div class="col-md-12">--}}
                {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="container">
                       <div class="order-track-info">
                           <div class="row margin-auto">
                               <div class="col-md-6">
                                   <div class="profile">
                                       <div class="media">
                                           <div class="media-left">
                                               <img src="{{asset('assets/img//profile.jpg')}}" class="media-object" width="75%" alt="">
                                           </div>
                                           <div class="media-body">
                                               <h4 class="media-heading">{{ $user->name }}</h4>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   {{--<div class="call-to-us text-right">--}}
                                       {{--<a href="#" data-toggle="modal" data-target="#call-us">Call Us</a>--}}
                                   {{--</div>--}}
                               </div>
                           </div>
                           <br>
                           <div class="contact-form-section">
                               <form action="{{ url('updateprofile') }}" method="POST" class="order-form" id="profile-form">
                                   @csrf
                                   <div class="row">
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <label for="name" class="form-label"> Your Name:</label>

                                               <input type="text" class="form-control cstm-input" name="name" maxlength="50" value="{{ $user->name }}" placeholder="YOUR NAME">
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group ">
                                               <label for="name" class="form-label">Your Phone</label>

                                               <input type="text" id="phone"  class="form-control cstm-input" value="{{ $user->mobile }}" placeholder="YOUR PHONE NUMBER" name="mobile"   minlength="11" maxlength="11" disabled="disabled">
                                               {{--<input type="hidden"  id="phone-number" name="mobile">--}}
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <label for="name" class="form-label"> Your Email:</label>
                                               <input class="form-control cstm-input" name="email" value="{{ $user->email }}" placeholder="YOUR EMAIL" maxlength="50">
                                           </div>
                                       </div>
                                       <div class="col-md-6">
                                           <div class="form-group">
                                               <label for="name" class="form-label">Your Address</label>
                                               <input type="text" class="form-control cstm-input" name="address" value="{{ $user->address }}" placeholder="YOUR ADDRESS" maxlength="250">
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="text-left">
                                       <button type="submit" class="btn btn-default order-submit"><span id="loader"></span>Update</button>
                                   </div>
                               </form>
                           </div>
            </div>
        </div>
    </div>
@endsection




