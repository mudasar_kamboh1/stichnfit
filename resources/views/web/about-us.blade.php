<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit - About us | Tailor-made Sewing and Alteration Services</title>
    <meta charset="UTF-8">
    <meta name="description" content="Stitch n Fit offers sewing and alteration services tailor-made to your requirements and sewed to perfection. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="online stitching, stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--about us banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/about-banner.png')}}" alt="AboutUs">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">About Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 {{--about us description section--}}
<section class="about-us-section">
    {{--<div class="container">--}}
        {{--<div class="col-md-12">--}}
            {{--<a href="{{ url()->previous() }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="container">
           <div class="row">
        <div class="col-md-12">
            <div class="about-description">
                <p class="text-justify">Stitch n Fit offers sewing and alteration services tailor-made to your requirements and sewed to perfection. We are sewing the gap between you and your tailor for all your sewing and alteration needs. Be it Eid, wedding, or any other occasion, we are here to save you the hassle of visiting tailors again and again for timely delivery. </p>
                <p class="text-justify">We are offering the perfect solution to all those lame excuses that you have to hear from the tailors particularly in the festive season. Give us a try and we will stitch your dream dress to perfection.</p>
            </div>
        </div>
    </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
{{--<script src="{{ asset('/assets/js/jquery.js') }}"></script>--}}
{{--<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>--}}
{{--// Including Footer Section--}}
@include('web.partials.assets')
</body>
</html>
