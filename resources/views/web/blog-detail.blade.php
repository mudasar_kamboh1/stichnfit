<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--Shirt gallery banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item2 active">
                <img class="d-block w-100" src="{{asset('assets/img/gallery.png')}}" alt="">
                <div class="carousel-caption cstm-caption">
                    <div class="col-md-12">
                        <h1 class="text-center">Blog</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="single-gallery-section">
    <div class="container">
    <div class="col-md-12">
     <a href="{{ url('blog') }}" class="btn back-btn"><i class="fas fa-angle-left"></i>&nbsp;&nbsp;Back </a>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 ">
            <div class="blog-description">
                <div class="row">
                <div class="col-md-12 text-left">
                        <img src="{{asset('/assets/uploads/'.$post->image)}}" class="img-responsive" alt="" width="100%">
                    <br><br>
                    <h2 class="mt-0 text-left">{{ !is_null($post) ? $post->name : '-- -- --' }}</h2>
                        <p class="text-justify">{!! !is_null($post) ? $post->description : '-- -- --' !!}</p>
                         </div>
                </div>
            </div>
            </div>
            <div class="col-md-4">
                @if(count($posts))
                        <div class="blog-description recent-blog">
                            <h5 class="text-center">RECENT BLOGS</h5>
                            <hr>
                            @foreach($posts as $blog)
                        <div class="row">
                            <div class="col-md-5">
                                <a href="{{ url('blog/'.$blog->slug) }}"><img src="{{asset('/assets/uploads/'.$blog->image)}}" class="img-responsive" alt="" width="100%"></a>
                            </div>
                            <div class="col-md-7">
                                <h5 class="mt-0 text-left"><a href="{{ url('blog/'.$blog->slug) }}">{{ !is_null($blog) ? $blog->name : '-- -- --' }}</a></h5>
                                <div class="news-tags text-left">
                                    <span class="date">{{\Carbon\Carbon::parse($blog->created_at)->format('d-m-Y') }}</span>
                                </div>
                            </div>
                    </div><br>
                            @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
