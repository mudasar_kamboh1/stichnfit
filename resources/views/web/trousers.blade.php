<!DOCTYPE html>
<html lang="en">
@section('metas')
    <title>Stitch n Fit | Online Tailoring & Sewing Services for Women</title>
    <meta charset="UTF-8">
    <meta name="description" content="Factory finished sewing and tailoring services for women. We collect your fabric, stitched to perfection as per your measurements and deliver it to your doorsteps. Get your sewing requirements fulfilled without stepping out of your house.">
    <meta name="keywords" content="stitching, online stitching, online Tailoring, Sewing Service, tailoring services for women, tailoring service, sewing needs">
@endsection
@include('web.partials.head')
<body>
{{--// Including Nav and Slide Section--}}
@include('web.partials.inner-page-nav')
{{--Trouser gallery banner section--}}
<section class="inner-banner-section">
    <div class="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="{{asset('assets/img/trouser-banner.jpg')}}" alt="">
                <div class="carousel-caption d-none d-md-block">
                    <div class="col-md-6">
                        <h1 class="text-right">Trousers</h1>
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Trousers</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--Trouser gallery section--}}
<section class="single-gallery-section">
    <div class="container">
        <div class="gallery-title text-center">
            <h3>Trousers</h3>
            <!-- <span class="heading-underline"></span> -->
        </div>
        <div class="gallery-items">
            <div class="row">
                <!-- gallery item 1 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/boot-cut-pants.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Boot-Cut Pants</h5>
                            <p class="card-text">Boot cut pants are the choice of today’s modern girl.
                                Get the perfect cut of your boot cut pants by the
                                specialized tailors at StichnFit.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 2 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/straight-pants.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Straight Pants</h5>
                            <p class="card-text">Because straight pants work anytime, anywhere.
                                Get the best ones stitched to your size from StichnFit
                                and rock any look you want to. </p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 3 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/tulip-shalwar.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Tulip Shalwar</h5>
                            <p class="card-text">Tulip shalwars can instantly lift up the look of your
                                shirts. Stand out with the tailor-made tulip shalwars.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 4 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/gharara-pants.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Gharara Pants</h5>
                            <p class="card-text">Gharara pants are everywhere these days. From
                                high end designers to the local shops, from casual
                                cotton ones to the fancy and banarsi ones, you can
                                get a gharara pant out of any fabric and for any
                                outfit.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 5 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/shalwar.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Shalwar</h5>
                            <p class="card-text">No matter how many other kinds of trousers and
                                pants become trending, shalwars simply work in all
                                ages and for all ages. From your naani and daadi
                                to your little girls, shalwar would go for just anyone.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 6 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/chooridaars.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Chooridaars</h5>
                            <p class="card-text">Chooridaars have become sort of our national
                                identity, and how elegant they look. Whether it’s a
                                long shirt you are wearing or a short frock, a
                                straight shirt or an angarkha, chooridaars make
                                a great pair with any of that.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
                <!-- gallery item 7 -->
                <div class="col-md-3">
                    <div class="card cstm-card">
                        <img class="card-img-top" src="{{asset('assets/img/customized-pants.png')}}">
                        <div class="card-body">
                            <h5 class="card-title">Customized</h5>
                            <p class="card-text">Have an idea of your own? No worries! Just
                                communicate your thoughts and ideas to our tailor
                                and they will turn them to reality, perfect to your
                                imagination.</p>
                            <a href="#" class="btn view-btn">View Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--// Including Footer Section--}}
@include('web.partials.footer')
@include('web.partials.assets')
</body>
</html>
